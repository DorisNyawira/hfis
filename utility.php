<?php
 session_start();
    require_once "Database.php";
    class utility extends connection{
        
        function login(){
            
            try{
                $id = $_POST['id'];
                $pass = $_POST['pass'];
                $result = $this->query("SELECT * FROM","users","WHERE IDNumber = ?",[$id]);
               if($result->rowCount() > 0){
                   $row = $result->fetch();
                   if(password_verify($pass, $row['Password'])){
                       
                        $_SESSION['fname'] = $row['FirstName'];
                        $_SESSION['rank'] = $row['rank'];
                        $_SESSION['IDNUMBER'] = $row['IDNumber'];
                        $_SESSION['FacilityID'] = $row['FacilityID'];
                     
                        $this->goToPage($row['rank']);
                    }else{ 
                       
                        echo "<script> alert('Wrong Password');window.open('healthdesigns/login.php','_self')</script>";
                    }
               }else{
                    echo "<script> alert('No such User PLease Check Your Credentials and Try Again');window.open('healthdesigns/login.php','_self')</script>";
               } 
                
                
            }catch(Exception $e){
                header("location:healthdesigns/login.php");
            }
           
        }
        
        function register(){
            $id = $_POST['id'];
            $fname = $_POST['fname'];
            $lname = $_POST['lname'];
            $jobid = $_POST['jobid'];
            $email = $_POST['email'];
            $pass = $_POST['pass'];
            @$confirm = $_POST['confirm'];
          
            
            $res = $this->query("SELECT * FROM","users","WHERE IDNumber = ?",[$id]);
            
            if($res->rowCount()>0){
                echo "<script> alert('User ID exists!');window.open('healthdesigns/Register.php','_self')</script>";}else{
                $res = $this->query("INSERT INTO ","users","(IDNumber,FirstName,LastName,JobID,EmailAddress,PassWord) VALUES(?,?,?,?,?,?)",[$id, $fname, $lname,$jobid,$email,password_hash($pass, PASSWORD_DEFAULT)]);
                echo "<script> alert('Registration was successfull!Please Wait for the Approval ,before you Login to your Account');window.open('healthdesigns/login.php','_self')</script>";
            }
        }
       
       
        function goToPage($rank){
          $FacilityID=$_SESSION['FacilityID'];
           $query = $this->connect()->prepare("select * from ".$this->database.".facilities where FacilityID =?");
         $query->execute([$FacilityID]);
        while($row = $query->fetch()){
                $_SESSION['FacilityName'] = $row['FacilityName'];
                }
            if($rank === "Facility Accountant"){
                header("location:healthdesigns/FacilityCollections.php?FacilityID=");
            }else if($rank === "AIE Holder"){
                header("location:healthdesigns/CommitmentSummary.php?FacilityID=");
            }else if($rank === "MOH/PMO"){
               header("location:healthdesigns/viewPendingReg.php?FacilityID=");
            }else{
               echo "<script> alert('Awaiting MOH/PMO Approval!');window.open('healthdesigns/login.php','_self')</script>";
                
            }
        }
        function pwdreset()
    {
        $email = $_POST['email'];
        $query = "SELECT * from " . $this->database . ".users WHERE EmailAddress='$email'";

        $result = $this->connect()->prepare($query);
        $result->execute([
            $email
        ]);

        if ($result->rowCount() > 0) {

            $keyLength = 8;
            $string = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM!#$?&';
            $newPassword = substr(str_shuffle($string), 0, $keyLength);
            $query = $this->connect()->prepare("UPDATE " . $this->database . ".users set Password=? WHERE EmailAddress='$email'");
            $query->execute([
                password_hash($newPassword, PASSWORD_DEFAULT)
            ]);

            $to = $email;
            $subject = "Reset your password";

            $msg = " You requested for a password reset for your account. Use the following password to log in to your account.You may change it to your preference(optionally).
                                Password:$newPassword";
            $msg = wordwrap($msg, 170);
            $headers = "From: vicdjprince001@gmail.com";
            mail($to, $subject, $msg, $headers);
            header("location:healthdesigns/pending.php?email=". $email);
        } else if ($result->rowCount() <= 0) {
            echo '<script>alert("No such email in our system");window.location="healthdesigns/forgotpassword.php"</script>';
       }
    
    }
       function resetPassword()
    {
        $email = $_POST['email'];
        $query = "SELECT * from " . $this->database . ".users WHERE EmailAddress='$email'";

        $result = $this->connect()->prepare($query);
     if ($result->rowCount() < 0) {
     echo'<script>alert("No such email in our system");window.location="forgotpassword.php" </script>';
       } else if ($result->rowcount() >0 ) {
         $token= bin2hex(random_bytes(50));
         $query=$this->connect()->prepare("insert into ".$this->database.".password_reset(email,token)values(?,?)");
         $query->execute([$email,$token]);
 
            $to = $email;
            $subject = "Reset your password";
            $msg = "Hi there,click on this<a href=\"resetPassword.php?token=" . $token . "\">link </a> to reset your password";
            $msg = wordwrap($msg, 70);
            $headers = "From: vicdjprince001@gmail.com";
            mail($to, $subject, $msg, $headers);
            header("location:healthdesigns/pending.php?email=". $email);
        }
    
    }
        
        function logout(){
            unset ($_COOKIE['id']);
            session_destroy();
            header("location:fis/index.html");
        }
        
        function report(){
            if(isset($_GET['suc'])){
                echo "<div class='success' id='r_success'>* ".$_GET['suc']."</div>";
            }
            if(isset($_GET['err'])){
                echo "<div class='error' id='r_error'>* ".$_GET['err']."</div>";
            }
        }
    
        function checklogin(){
            if(isset($_SESSION['rank'])){
                return true;
            }
            return false;
        }
        
        function check($rank){
            if(isset($_SESSION['rank']) && $_SESSION['rank']==$rank){
                
            }else{
                $this->logout();
            }
        }
    }

    $utility = new utility();


    if(isset($_POST['register'])){
        $utility->register();
    }

    if(isset($_POST['login'])){
        
        $utility->login();
    }
     if(isset($_POST['pwdreset'])){
        $utility->pwdreset();
    }

    if(isset($_POST['resetPassword'])){
        $utility->resetPassword();
    }
     if(isset($_POST['logout'])){
        $utility->logout();
    }

?>