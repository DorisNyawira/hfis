(function() {

    var __hs_cta_json = {"css":"a#cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1 {\n    -webkit-font-smoothing: antialiased;\ncursor: pointer;\n-moz-user-select: none;\n-webkit-user-select: none;\n-o-user-select: none;\nuser-select: none;\ndisplay: inline-block;\nfont-weight: normal;\ntext-align: center;\ntext-decoration: none;\n-moz-transition: all .4s ease;\n-webkit-transition: all .4s ease;\n-o-transition: all .4s ease;\nbackground: rgb(151,215,0);\nborder-radius: 6px;\nborder-width: 0px;\ncolor: rgb(255,255,255);\nfont-family: sans-serif;\nheight: auto;\ntransition: all .4s ease;\npadding: 16px 33px;\ntext-shadow: none;\nwidth: auto;\nline-height: 1.5em;\n\n    \n  }\n\na#cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1:hover {\nbackground: rgb(166,236,0);\ncolor: rgb(255,255,255);\n}\n\na#cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1:active, #cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1:active:hover {\nbackground: rgb(120,172,0);\ncolor: rgb(244,244,244);\n}\n\na#cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1 {\n  -webkit-font-smoothing:antialiased; \n  cursor:pointer; \n  -moz-user-select:none; \n  -webkit-user-select:none; \n  -o-user-select:none; \n  user-select:none; \n  display:inline-block; \n  font-weight:normal; \n  text-align:center; \n  text-decoration:none; \n  font-family:sans-serif; \n  color:rgb(255, 255, 255); \n  border-radius:5px; \n  transition:all .4s ease; \n  -moz-transition:all .4s ease; \n  -webkit-transition:all .4s ease; \n  -o-transition:all .4s ease; \n  text-shadow:none; \n  line-height:1.5em; \n  padding:16px 33px; \n}","css_links":[],"image_html":"<a id=\"cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1\" class=\"cta_button\" href=\"https://www.lumitex.com/cs/c/?cta_guid=c5e130d0-88f5-49d1-b51f-2a324a7bcbd1&signature=AAH58kHkUkNMO9GmtaST4rveoAPEzth4dg&pageId=6653958318&placement_guid=e203dea1-722e-4116-ae3b-ab167b548b75&click=08056460-3459-46b9-ad64-f7b1ab533289&hsutk=021db8a76605abdbea131ea18fb58efa&canon=https%3A%2F%2Fwww.lumitex.com%2Fblog%2Frobotic-surgery&utm_referrer=https%3A%2F%2Fwww.google.com%2F&portal_id=2606137&contentType=blog-post&redirect_url=APefjpFDQeqhzYVwNbhnjE-gPGqlhpvfFMdtEFPo3W3PgkMWuKTJ2SGpEp9DPJ2t72kdTdRHGQw3hNL7te-eMnueB4tbHUbcLJC0UFeoPLWehO7lVTQ3enlwfDSO5lKBQ732ypWhj_6HEZ7MYcnFE7lZUv8SZGGUJQIH2BPbSjWhAVMWOPS3iEa5i06ehozemxTx5DnC3mQwTRkCej-JCRe-W_S7ljMwLCtUWO1YaDEfAtLPanOZ_AYt0tGfcPuOzWL_J8tqQHsAQIPQyuOGDTsvwoXELgr4Nw\"  cta_dest_link=\"https://www.lumitex.com/offer/robotic-surgery\"><img id=\"hs-cta-img-e203dea1-722e-4116-ae3b-ab167b548b75\" class=\"hs-cta-img \" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"Yes! Give me my PDF\" src=\"https://cdn2.hubspot.net/hubshot/19/05/09/3f1cb691-ae65-4a6a-98c0-4f7966c6ecc8.png\" srcset=\"https://cdn2.hubspot.net/hubshot/19/05/09/0e05594e-5360-4642-a074-6b773479600e.png 4x, https://cdn2.hubspot.net/hubshot/19/05/09/f0d1e1bd-3346-4ecd-898c-40d81ecf9f88.png 3x, https://cdn2.hubspot.net/hubshot/19/05/09/f815d230-f52a-4c79-9707-6e381091324d.png 2x\"/></a>","is_image":false,"placement_element_class":"hs-cta-e203dea1-722e-4116-ae3b-ab167b548b75","raw_html":"<a id=\"cta_button_2606137_c5e130d0-88f5-49d1-b51f-2a324a7bcbd1\" class=\"cta_button \" href=\"https://www.lumitex.com/cs/c/?cta_guid=c5e130d0-88f5-49d1-b51f-2a324a7bcbd1&signature=AAH58kHkUkNMO9GmtaST4rveoAPEzth4dg&pageId=6653958318&placement_guid=e203dea1-722e-4116-ae3b-ab167b548b75&click=08056460-3459-46b9-ad64-f7b1ab533289&hsutk=021db8a76605abdbea131ea18fb58efa&canon=https%3A%2F%2Fwww.lumitex.com%2Fblog%2Frobotic-surgery&utm_referrer=https%3A%2F%2Fwww.google.com%2F&portal_id=2606137&contentType=blog-post&redirect_url=APefjpFDQeqhzYVwNbhnjE-gPGqlhpvfFMdtEFPo3W3PgkMWuKTJ2SGpEp9DPJ2t72kdTdRHGQw3hNL7te-eMnueB4tbHUbcLJC0UFeoPLWehO7lVTQ3enlwfDSO5lKBQ732ypWhj_6HEZ7MYcnFE7lZUv8SZGGUJQIH2BPbSjWhAVMWOPS3iEa5i06ehozemxTx5DnC3mQwTRkCej-JCRe-W_S7ljMwLCtUWO1YaDEfAtLPanOZ_AYt0tGfcPuOzWL_J8tqQHsAQIPQyuOGDTsvwoXELgr4Nw\"  style=\"/*hs-extra-styles*/\" cta_dest_link=\"https://www.lumitex.com/offer/robotic-surgery\" title=\"Yes! Give me my PDF\"><strong><span style=\"color: #ffffff; font-size: 24px; font-family: sans-serif; line-height: 1.5em;\">Yes! Give me my PDF</span></strong></a>"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }
            
            html = __hs_cta_json.raw_html;

        }
         

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad('e203dea1-722e-4116-ae3b-ab167b548b75');
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json.placement_element_class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json.placement_element_class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
    
    window._hsq = window._hsq || [];
    window._hsq.push(['trackCtaView', 'e203dea1-722e-4116-ae3b-ab167b548b75', 'c5e130d0-88f5-49d1-b51f-2a324a7bcbd1']);
    

}());
