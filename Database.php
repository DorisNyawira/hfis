<?php

class connection{
    private $server = "localhost";
    protected $database ="hfis";
    private $charset ="utf8";
    private $username = "root";
    private $pwd = "";
    //define the methods to error
    public $emailAddress = "";


    protected function connect(){
     try {
        $dsn= "mysql:host=".$this->server.";dbname=".$this->database.";charset".$this->charset;

        $pdo_conn= new PDO($dsn, $this->username, $this->pwd);

        $pdo_conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);


        return $pdo_conn;
            
        } 
        catch (PDOException $e) {
            echo "Connection Failed: ". $e->getMessage();
            die;
            
        }   
    }
     public function query($query1, $tableName, $query2, $array){

            $query = $query1." ".$this->database.".".$tableName." ".$query2;
            $run = $this->connect()->prepare($query);

            try{
                $run->execute($array);
                return $run;
            }catch(Exception $ex){
                echo $ex;
                return false;
            }
        }
        public function getuserdetails(){
            $IDNumber = $_GET['IDNumber'];
        
    $query = $this->connect()->prepare("select * from ".$this->database.".users where IDNumber =? ");
        $query->execute([$IDNumber]);
        while($row = $query->fetch()){
            
        echo "
                <div class='form-group row'>
                 
                  <div class='col-sm-6'>
                    <input type='text' class='form-control form-control-user' name='fname' id='fname' value=".$row['FirstName'].">
                     <input type='hidden' class='form-control form-control-user' name='IDNumber' value=".$row['IDNumber'].">
                  </div>
                    <div class='col-sm-6'>
                    <input type='text' class='form-control form-control-user' name='lname' id'lname' value=".$row['LastName'].">
                  </div>
                </div>
                 
                <div class='form-group'>
                  <input type='email' class='form-control form-control-user' name='email' id='exampleInputEmail' value=".$row['EmailAddress'].">
                </div>
                  
                <div class='form-group row'>
                  <div class='col-sm-6 mb-3 mb-sm-0'>
                    <input type='password' class='form-control form-control-user' name='pass' id='exampleInputPassword' placeholder='Password'>
                  </div>
                  <div class='col-sm-6'>
                    <input type='password' class='form-control form-control-user' name='cpass' id='exampleRepeatPassword' placeholder='Repeat Password'>
                  </div>
                </div>
                
             <hr>
            
            ";
        }
    }
     public function updateuserdetails(){
         
         session_start();
            $IDNumber = $_POST['IDNumber'];
            $fname = $_POST['fname'];
            $lname = $_POST['lname'];
            $email = $_POST['email'];
            $pass = $_POST['pass'];
           // @$confirm = $_POST['confirm'];
    
    $query = $this->connect()->prepare("UPDATE ".$this->database.".users SET FirstName = ?,LastName = ?,EmailAddress = ?,Password = ? WHERE IDNumber= ? ");
        $query->execute([$fname,$lname,$email,password_hash($pass, PASSWORD_DEFAULT),$IDNumber]);
        echo "<script> alert('$fname profile added successfully');window.open('healthdesigns/login.php','_self')</script>";
    }
    
   
  public function addFacility(){
   
     $FacilityID = $_POST['FacilityID'];  
     $FacilityName= $_POST['FacilityName'];
 $query = $this->connect()->prepare("insert into ".$this->database.".facilities(FacilityID,FacilityName) values (?,?)");
        $query->execute([$FacilityID, $FacilityName]);
        echo "<script> alert('$FacilityName added successfully');window.open('healthdesigns/addFacility.php','_self')</script>";
    }
    public function getfacilities(){
       
        
        $query = $this->connect()->prepare("select * from ".$this->database.".facilities");
        $query->execute(); 
        
        while($row = $query->fetch()){
             echo
                
                "<option value=".$row['FacilityID'].">".$row['FacilityName']."</option>";
     
        }
        
    }
    
    public function getSystemUsers(){
       
        
        $query = $this->connect()->prepare("select * from ".$this->database.".users where FacilityID is NULL");
        $query->execute(); 
        
        while($row = $query->fetch()){
             echo
                
                "<option value=".$row['IDNumber'].">".$row['EmailAddress']."</option>";
     
        }
    }
        
         public function getAndDisplayUsers(){
             
             if(empty($_GET['FacilityID'])){
                 $FacilityID = null;
             } else{
                  $FacilityID = $_GET['FacilityID']; 
             }
             
        $query = $this->connect()->prepare("select * from ".$this->database.".users  where FacilityID = ?");
        $query->execute([$FacilityID]); 
        
        while($row = $query->fetch()){
             echo "<tr>
                
                <td>" .$row['EmailAddress']."</td>
                <td>" .$row['JobID']."</td>
                <td>" .$row['rank']."</td>
                  <td>
                  <a href='#' class='btn btn-success btn-sm'    id=".$row['IDNumber']." email=".$row['EmailAddress']." data-toggle='modal' data-target='#editUser' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                 
                  <a href='#' class='btn btn-danger btn-sm'    id=".$row['IDNumber']."  data-toggle='modal' data-target='#deleteUser' >
                  <input type='hidden' name='FacilityID' value = $FacilityID />
                    <span class='glyphicon glyphicon-trash'></span> Delete
                     </a>     
                  </td>

            </tr>";
        
        }
        
        
    
}
    public function editRankUser(){
     session_start();
     $rank = $_POST['rank'];
     $FacilityID = $_POST['FacilityID'];
    $IDNumber = $_POST['userID'];
    
        
 
    $query = $this->connect()->prepare("UPDATE ".$this->database.".users SET FacilityID =?,rank = ? WHERE IDNumber= ?");
        $query->execute([$FacilityID,$rank,$IDNumber]);
        echo "<script> alert('User edited  successfully');window.open('healthdesigns/assignFacilities.php','_self')</script>";
    
        
    }
     public function deleteUser(){
         $IDNumber =  $_POST['userID'];
     $query = $this->connect()->prepare(" DELETE FROM ".$this->database.".users WHERE IDNumber =? ");
        $query->execute([$IDNumber]);
        echo "<script> alert('user deleted successfully');window.open('healthdesigns/assignFacilities.php','_self')</script>";
        
        
    }
    
    public function assignUserFacility(){
     session_start();
     $FacilityID = $_POST['FacilityIDs'];
     $UserID = $_POST['UserIDNumber'];
        
 
    $query = $this->connect()->prepare("UPDATE ".$this->database.".users SET FacilityID =? WHERE IDNumber= ?");
        $query->execute([$FacilityID,$UserID]);
        echo "<script> alert('Assigned facility  successfully');window.open('healthdesigns/assignFacilities.php','_self')</script>";
    }
    
    
    
    
    
  public function addbankreconciliation(){
     session_start();
     $FacilityID = $_POST['FacilityID'];
     $date= $_POST['date'];
     $payee= $_POST['payee'];
     $amount= $_POST['amount'];
     $details= $_POST['details'];
     $approvalID= $_POST['approval_id'];
        
 $query = $this->connect()->prepare("insert into ".$this->database.".bankreconciliation(FacilityID,date,payee,amount,details,approval_id) values (?,?,?,?,?,?)");
        $query->execute([$FacilityID, $date, $payee, $amount, $details,$approvalID]);
        echo "<script> alert('Records added successfully');window.open('healthdesigns/BankReconciliation.php?FacilityID=$FacilityID','_self')</script>";
    }
     public function addcashbook(){
     session_start();
        $FacilityID = $_POST['FacilityID'];
        $bankstmtdate= $_POST['bankstmtdate'];
        $cashbookbalance = $_POST['cashbookbalance'];
        $totaldeposits= $_POST['totaldeposits'];
        $expenditures= $_POST['expenditures'];
        $bankcharges= $_POST['bankcharges'];
        $totalwithdrawals= $_POST['totalwithdrawals'];
        $bankstmtbalance= $_POST['bankstmtbalance'];
        $approvalID= $_POST['approval_id'];
       
 $query = $this->connect()->prepare("insert into ".$this->database.".cashbook(FacilityID,bankstmtdate,cashbookbalance,totaldeposits,expenditures,bankcharges,totalwithdrawals,bankstmtbalance,approval_id) values (?,?,?,?,?,?,?,?,?)");
        $query->execute([$FacilityID,$bankstmtdate,$cashbookbalance,$totaldeposits,$expenditures,$bankcharges,$totalwithdrawals,$bankstmtbalance,$approvalID]);
        echo "<script> alert('Records added successfully');window.open('healthdesigns/CashBook.php?FacilityID=$FacilityID','_self')</script>";
   
    }
 public function addFacilityCollections(){
     session_start();
      $FacilityID = $_POST['FacilityID'];
        $Date = $_POST['Date'];
        $BedandProcedures= $_POST['BedandProcedures'];
        $Maternity= $_POST['Maternity'];
        $Xray= $_POST['Xray'];
        $Lab = $_POST['Lab'];
        $Theatre= $_POST['Theatre'];
        $Mortuary= $_POST['Mortuary'];
        $OPTreatment= $_POST['OPTreatment'];
        $Pharmacy= $_POST['Pharmacy'];
        $approvalID= $_POST['approval_id'];
        
       
        
 $query = $this->connect()->prepare("insert into ".$this->database.".facilitycollections(FacilityID, Date,BedandProcedures,Maternity,Xray,Lab,Theatre,Mortuary,OPTreatment,Pharmacy,approval_id) values (?,?,?,?,?,?,?,?,?,?,?)");
        $query->execute([$FacilityID, $Date,$BedandProcedures, $Maternity, $Xray, $Lab, $Theatre, $Mortuary, $OPTreatment, $Pharmacy,$approvalID]);
        echo "<script> alert('Records added successfully');window.open('healthdesigns/FacilityCollections.php?FacilityID=$FacilityID','_self')</script>";
    }
    public function addpaymentsandcommitmentsummary(){
     session_start();
        $FacilityID = $_POST['FacilityID'];
        $itemcode = $_POST['itemcode'];
        $itemsubcode= $_POST['itemsubcode'];
        $itemdescription= $_POST['itemdescription'];
        $payments = $_POST['payments'];
        $cumulativepayments= $_POST['cumulativepayments'];
        $newcommitments= $_POST['newcommitments'];
        $outstandingcommitments= $_POST['outstandingcommitments'];
        $balance= $_POST['balance'];
        $approvalID= $_POST['approval_id'];
        $date= $_POST['date'];
       
   
        
 $query = $this->connect()->prepare("insert into ".$this->database.".paymentandcommitmentsummary(FacilityID,itemcode,itemsubcode,itemdescription,payments,cumulativepayments,newcommitments,outstandingcommitments,balance,approval_id,dateofsummary) values (?,?,?,?,?,?,?,?,?,?,?)");
        $query->execute([$FacilityID,$itemcode,$itemsubcode,$itemdescription,$payments,$cumulativepayments, $newcommitments, $outstandingcommitments, $balance,$approvalID,$date]);
        echo "<script> alert('Records added successfully');window.open('healthdesigns/CommitmentSummary.php?FacilityID=$FacilityID','_self')</script>";
   
    }
        public function getFacilityCollections(){
       $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".facilitycollections where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                
                
                <td>" .$row['Date']."</td>
                <td>" .$row['BedandProcedures']."</td>
                <td>" .$row['Maternity']."</td>
                <td>" .$row['Xray']."</td>
                <td>" .$row['Lab']."</td>
                <td>"  .$row['Theatre']."</td>
                <td>" .$row['Mortuary']."</td>
                 <td>" .$row['OPTreatment']."</td>
                 <td>" .$row['Pharmacy']."</td>
                 
                  <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." date=".$row['Date']." BedandProcedures=".$row['BedandProcedures']."  Maternity=".$row['Maternity']." Xray=".$row['Xray']."   Lab=".$row['Lab']." Theatre=".$row['Theatre']."  Mortuary=".$row['Mortuary']." OPTreatment=".$row['OPTreatment']." Pharmacy=".$row['Pharmacy']." data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>

            </tr>";
          
        }
       
    }
                     
    public function getFacilityCollectionsAdmin(){
      $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".facilitycollections where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                
                
                <td>" .$row['Date']."</td>
           
                <td>" .$row['BedandProcedures']."</td>
                <td>" .$row['Maternity']."</td>
                <td>" .$row['Xray']."</td>
                <td>" .$row['Lab']."</td>
                <td>"  .$row['Theatre']."</td>
                <td>" .$row['Mortuary']."</td>
                 <td>" .$row['OPTreatment']."</td>
                 <td>" .$row['Pharmacy']."</td>
                  <td>
                  <div class='mt-4 mb-2'>
                <a href='#' class='btn btn-danger btn-circle btn-sm'   declineid=".$row['id']." data-toggle='modal' data-target='#declineModal' >
                     <i class='fas fa-trash'></i>
                     </a>
                       
                  <a href='#' class='btn btn-success btn-circle btn-sm'   approveid=".$row['id']." data-toggle='modal' data-target='#approveModal' >
                     <i class='fas fa-check'></i>
                     </a>
                  </div>
                  </td>
           </tr>";
           
        }
    }
       public function getFacilityCollectionName(){
      $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".facilitycollections where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                
                
                <td>" .$row['Date']."</td>
           
                <td>" .$row['BedandProcedures']."</td>
                <td>" .$row['Maternity']."</td>
                <td>" .$row['Xray']."</td>
                <td>" .$row['Lab']."</td>
                <td>"  .$row['Theatre']."</td>
                <td>" .$row['Mortuary']."</td>
                 <td>" .$row['OPTreatment']."</td>
                 <td>" .$row['Pharmacy']."</td>
                  <td>
                  <div class='mt-4 mb-2'>
                  
                 
                  
                  <a href='#' class='btn btn-danger btn-circle btn-sm'   declineid=".$row['id']." data-toggle='modal' data-target='#declineModal' >
                     <i class='fas fa-trash'></i>
                     </a>
                       
                  <a href='#' class='btn btn-success btn-circle btn-sm'   approveid=".$row['id']." data-toggle='modal' data-target='#approveModal' >
                     <i class='fas fa-check'></i>
                     </a>
                       
                  
                     
                 
                  </div>
                  </td>
                  
                  
               
                
            </tr>";
           
        }
    }
    public function getFacilityCollectionsDecline(){
        $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".facilitycollections where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                
                
                <td>" .$row['Date']."</td>
           
                <td>" .$row['BedandProcedures']."</td>
                <td>" .$row['Maternity']."</td>
                <td>" .$row['Xray']."</td>
                <td>" .$row['Lab']."</td>
                <td>"  .$row['Theatre']."</td>
                <td>" .$row['Mortuary']."</td>
                 <td>" .$row['OPTreatment']."</td>
                 <td>" .$row['Pharmacy']."</td>
                <td>" .$row['Comments']."</td>
           </tr>";
           
        }
       
    }
    public function getFacilityCollectionsDeclinefa(){
        $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".facilitycollections where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                
                
                <td>" .$row['Date']."</td>
           
                <td>" .$row['BedandProcedures']."</td>
                <td>" .$row['Maternity']."</td>
                <td>" .$row['Xray']."</td>
                <td>" .$row['Lab']."</td>
                <td>"  .$row['Theatre']."</td>
                <td>" .$row['Mortuary']."</td>
                 <td>" .$row['OPTreatment']."</td>
                 <td>" .$row['Pharmacy']."</td>
                <td>" .$row['Comments']."</td>
                   
                  <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." date=".$row['Date']." BedandProcedures=".$row['BedandProcedures']."  Maternity=".$row['Maternity']." Xray=".$row['Xray']."   Lab=".$row['Lab']." Theatre=".$row['Theatre']."  Mortuary=".$row['Mortuary']." OPTreatment=".$row['OPTreatment']." Pharmacy=".$row['Pharmacy']." data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>
                  
               
                
            </tr>";
           
        }
       
    }
     public function getFacilityCollectionsApprove(){
       $FacilityID = $_GET['FacilityID'];
         $fromDate = $_GET['fromDate'];
         $toDate = $_GET['toDate'];
        $query = $this->connect()->prepare("select * from ".$this->database.".facilitycollections where FacilityID =? AND approval_id = 1 AND Date between ? AND ? ");
       $query->execute([$FacilityID,$fromDate,$toDate]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                
                
                <td>" .$row['Date']."</td>
           
                <td>" .$row['BedandProcedures']."</td>
                <td>" .$row['Maternity']."</td>
                <td>" .$row['Xray']."</td>
                <td>" .$row['Lab']."</td>
                <td>"  .$row['Theatre']."</td>
                <td>" .$row['Mortuary']."</td>
                 <td>" .$row['OPTreatment']."</td>
                 <td>" .$row['Pharmacy']."</td>
             
            </tr>";
           
        }
  
        }
    public function getcashbook(){
       $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".cashbook where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            
            echo "<tr>
                <td>"  .$row['bankstmtdate']."</td>
                <td>" .$row['cashbookbalance']."</td>
                <td>" .$row['totaldeposits']."</td>
                <td>" .$row['expenditures']."</td>
               <td>" .$row['bankcharges']."</td>
                 <td>" .$row['totalwithdrawals']."</td>
                   
                <td>" .$row['bankstmtbalance']."</td>
                    <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." bankstmtdate=".$row['bankstmtdate']." cashbookbalance=".$row['cashbookbalance']."  totaldeposits=".$row['totaldeposits']." expenditures=".$row['expenditures']."   bankcharges=".$row['bankcharges']." totalwithdrawals=".$row['totalwithdrawals']."  bankstmtbalance=".$row['bankstmtbalance']." data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>      
            </tr>";
        }  
      
         }
     public function getcashbookAdmin(){
         $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".cashbook where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            
            echo "<tr>
                <td>"  .$row['bankstmtdate']."</td>
                <td>" .$row['cashbookbalance']."</td>
                <td>" .$row['totaldeposits']."</td>
                <td>" .$row['expenditures']."</td>
               <td>" .$row['bankcharges']."</td>
                 <td>" .$row['totalwithdrawals']."</td>
                   
                <td>" .$row['bankstmtbalance']."</td>
                
                   <td>
                  <div class='mt-4 mb-2'>
                
                  <a href='#' class='btn btn-danger btn-circle btn-sm'   declineid=".$row['id']." data-toggle='modal' data-target='#declineModal' >
                     <i class='fas fa-trash'></i>
                     </a>
                       
                  <a href='#' class='btn btn-success btn-circle btn-sm'   approveid=".$row['id']." data-toggle='modal' data-target='#approveModal' >
                     <i class='fas fa-check'></i>
                     </a>
                    
                  </div>
                  </td>
                 
            </tr>";
        }  
      
         }
         public function getcashbookApprove() {
            $FacilityID = $_GET['FacilityID'];
                 $fromDate = $_GET['fromDate'];
                $toDate = $_GET['toDate'];
        $query = $this->connect()->prepare("select * from ".$this->database.".cashbook where FacilityID =? AND approval_id = 1 AND 	bankstmtdate between ? AND ? ");
       $query->execute([$FacilityID,$fromDate,$toDate]);
        while($row = $query->fetch()){
            
            echo "<tr>
                <td>"  .$row['bankstmtdate']."</td>
                <td>" .$row['cashbookbalance']."</td>
                <td>" .$row['totaldeposits']."</td>
                <td>" .$row['expenditures']."</td>
               <td>" .$row['bankcharges']."</td>
                 <td>" .$row['totalwithdrawals']."</td>    
                <td>" .$row['bankstmtbalance']."</td>
              
            </tr>";
        }  
         }
              public function getcashbookDeclinefa() {
         $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".cashbook where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            
            echo "<tr>
                <td>"  .$row['bankstmtdate']."</td>
                <td>" .$row['cashbookbalance']."</td>
                <td>" .$row['totaldeposits']."</td>
                <td>" .$row['expenditures']."</td>
               <td>" .$row['bankcharges']."</td>
                 <td>" .$row['totalwithdrawals']."</td>    
                <td>" .$row['bankstmtbalance']."</td>
                 <td>" .$row['Comments']."</td>
                     
                  <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." bankstmtdate=".$row['bankstmtdate']." cashbookbalance=".$row['cashbookbalance']."  totaldeposits=".$row['totaldeposits']." expenditures=".$row['expenditures']."   bankcharges=".$row['bankcharges']." totalwithdrawals=".$row['totalwithdrawals']."  bankstmtbalance=".$row['bankstmtbalance']." data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td> 
            </tr>";
        }  
         }
          public function getcashbookDecline() {
         $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".cashbook where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            
            echo "<tr>
                <td>"  .$row['bankstmtdate']."</td>
                <td>" .$row['cashbookbalance']."</td>
                <td>" .$row['totaldeposits']."</td>
                <td>" .$row['expenditures']."</td>
               <td>" .$row['bankcharges']."</td>
                 <td>" .$row['totalwithdrawals']."</td>    
                <td>" .$row['bankstmtbalance']."</td>
                 <td>" .$row['Comments']."</td>
            </tr>";
        }  
         }
            public function getbankreconciliation(){
           $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".bankreconciliation where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['date']."</td>
                <td>" .$row['payee']."</td>
                <td>" .$row['amount']."</td>
                <td>" .$row['details']."</td>
                      <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." date=".$row['date']." payee=".$row['payee']."  amount=".$row['amount']." details=".$row['details']."  data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>
                       </tr>";
        }  
         }
           public function getbankreconciliationAdmin(){
         $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".bankreconciliation where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['date']."</td>
                <td>" .$row['payee']."</td>
                <td>" .$row['amount']."</td>
                <td>" .$row['details']."</td>
                     <td>
                  <div class='mt-4 mb-2'>
                
                  <a href='#' class='btn btn-danger btn-circle btn-sm'   declineid=".$row['id']." data-toggle='modal' data-target='#declineModal' >
                     <i class='fas fa-trash'></i>
                     </a>
                       
                  <a href='#' class='btn btn-success btn-circle btn-sm'   approveid=".$row['id']." data-toggle='modal' data-target='#approveModal' >
                     <i class='fas fa-check'></i>
                     </a>
                    
                  </div>
                  </td>
               
            </tr>";
        }  
     
         }
         public function getBankReconciliationApprove(){
              $FacilityID = $_GET['FacilityID'];
                 $fromDate = $_GET['fromDate'];
                $toDate = $_GET['toDate'];
        $query = $this->connect()->prepare("select * from ".$this->database.".bankreconciliation where FacilityID =? AND approval_id = 1 AND 	date between ? AND ? ");
       $query->execute([$FacilityID,$fromDate,$toDate]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['date']."</td>
                <td>" .$row['payee']."</td>
                <td>" .$row['amount']."</td>
                <td>" .$row['details']."</td>
                 
               
            </tr>";
        }  
         }
              public function getBankReconciliationDeclinefa(){
             $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".bankreconciliation where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['date']."</td>
                <td>" .$row['payee']."</td>
                <td>" .$row['amount']."</td>
                <td>" .$row['details']."</td>
                <td>" .$row['Comments']."</td>
                   <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." date=".$row['date']." payee=".$row['payee']."  amount=".$row['amount']." details=".$row['details']."  data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>
            </tr>";
        }  
     
         }
                  public function getBankReconciliationDecline(){
             $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".bankreconciliation where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['date']."</td>
                <td>" .$row['payee']."</td>
                <td>" .$row['amount']."</td>
                <td>" .$row['details']."</td>
                <td>" .$row['Comments']."</td>
               
            </tr>";
        }  
     
         }
         

  public function getpaymentsandcommitmentsummary(){
              $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".paymentandcommitmentsummary where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['itemcode']."</td>
                <td>" .$row['itemsubcode']."</td>
                <td>" .$row['itemdescription']."</td>
                <td>" .$row['payments']."</td>
                <td>"  .$row['cumulativepayments']."</td>
                <td>" .$row['newcommitments']."</td>
                 <td>" .$row['outstandingcommitments']."</td>
                 <td>" .$row['balance']."</td>  
                 <td>" .$row['dateofsummary']."</td> 
                 
        <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." itemcode=".$row['itemcode']." itemsubcode=".$row['itemsubcode']."  itemdescription=".$row['itemdescription']."  date=".$row['dateofsummary']." payments=".$row['payments']."   cumulativepayments=".$row['cumulativepayments']." newcommitments=".$row['newcommitments']."  outstandingcommitments=".$row['outstandingcommitments']." balance=".$row['balance']." data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>
            </tr>";
        }  
     
         }
     
    public function getpaymentsandcommitmentsummaryAdmin(){
             $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".paymentandcommitmentsummary where FacilityID =? and approval_id = 0");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['itemcode']."</td>
                <td>" .$row['itemsubcode']."</td>
                <td>" .$row['itemdescription']."</td>
                <td>" .$row['payments']."</td>
                <td>"  .$row['cumulativepayments']."</td>
                <td>" .$row['newcommitments']."</td>
                 <td>" .$row['outstandingcommitments']."</td>
                 <td>" .$row['dateofsummary']."</td> 
                 <td>" .$row['balance']."</td>   

                   <td>
                  <div class='mt-4 mb-2'>
                
                  <a href='#' class='btn btn-danger btn-circle btn-sm'   declineid=".$row['id']." data-toggle='modal' data-target='#declineModal' >
                     <i class='fas fa-trash'></i>
                     </a>
                       
                  <a href='#' class='btn btn-success btn-circle btn-sm'   approveid=".$row['id']." data-toggle='modal' data-target='#approveModal' >
                     <i class='fas fa-check'></i>
                     </a>
                    
                  </div>
                  </td>
                  
            </tr>";
        }  
      
         }
         public function getpaymentsandcommitmentsummaryApprove() {
                $fromDate = $_GET['fromDate'];
                $toDate = $_GET['toDate'];
               $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".paymentandcommitmentsummary where FacilityID =? AND approval_id = 1 AND 	dateofsummary between ? AND ? ");
       $query->execute([$FacilityID,$fromDate,$toDate]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['itemcode']."</td>
                <td>" .$row['itemsubcode']."</td>
                <td>" .$row['itemdescription']."</td>
                <td>" .$row['payments']."</td>
                <td>"  .$row['cumulativepayments']."</td>
                <td>" .$row['newcommitments']."</td>
                 <td>" .$row['outstandingcommitments']."</td>
                 <td>" .$row['balance']."</td> 
                 <td>" .$row['dateofsummary']."</td> 
            </tr>";
        }  
      
             
         } 
             public function getpaymentsandcommitmentsummaryDeclineah() {
             $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".paymentandcommitmentsummary where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['itemcode']."</td>
                <td>" .$row['itemsubcode']."</td>
                <td>" .$row['itemdescription']."</td>
                <td>" .$row['payments']."</td>
                <td>"  .$row['cumulativepayments']."</td>
                <td>" .$row['newcommitments']."</td>
                 <td>" .$row['outstandingcommitments']."</td>
                 <td>" .$row['balance']."</td> 
                 <td>" .$row['dateofsummary']."</td> 
                 <td>" .$row['Comments']."</td>
                     
                       <td>
                  <a href='#' class='btn btn-primary btn-sm'    id=".$row['id']." itemcode=".$row['itemcode']." itemsubcode=".$row['itemsubcode']."  itemdescription=".$row['itemdescription']."  date=".$row['dateofsummary']." payments=".$row['payments']."   cumulativepayments=".$row['cumulativepayments']." newcommitments=".$row['newcommitments']."  outstandingcommitments=".$row['outstandingcommitments']." balance=".$row['balance']." data-toggle='modal' data-target='#editModal' >
                    <span class='glyphicon glyphicon-edit'></span> Edit
                     </a>     
                  </td>
            </tr>";
        }  
      } 
         public function getpaymentsandcommitmentsummaryDecline() {
             $FacilityID = $_GET['FacilityID'];
        $query = $this->connect()->prepare("select * from ".$this->database.".paymentandcommitmentsummary where FacilityID =? and approval_id = 2");
       $query->execute([$FacilityID]);
        while($row = $query->fetch()){
            echo "<tr>
                <form action='Database.php' method='post'>
                <td>" .$row['itemcode']."</td>
                <td>" .$row['itemsubcode']."</td>
                <td>" .$row['itemdescription']."</td>
                <td>" .$row['payments']."</td>
                <td>"  .$row['cumulativepayments']."</td>
                <td>" .$row['newcommitments']."</td>
                 <td>" .$row['outstandingcommitments']."</td>
                 <td>" .$row['balance']."</td> 
                 <td>" .$row['dateofsummary']."</td> 
                 <td>" .$row['Comments']."</td>
            </tr>";
        }  
      } 
             
     
         
            
        public function editFacilityCollections(){
        $id = $_POST['id'];    
        $Date = $_POST['date'];
        $BedandProcedures= $_POST['BedandProcedures'];
        $Maternity= $_POST['Maternity'];
        $Xray= $_POST['Xray'];
        $Lab = $_POST['Lab'];
        $Theatre= $_POST['Theatre'];
        $Mortuary= $_POST['Mortuary'];
        $OPTreatment= $_POST['OPTreatment'];
        $Pharmacy= $_POST['Pharmacy'];
        $FacilityID=$_POST['FacilityID'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".facilitycollections SET date =?,BedandProcedures =?, Maternity =?,Xray =?,Lab =?,Theatre =?,Mortuary =?,OPTreatment =?,Pharmacy =? ,approval_id =0 WHERE id= ?");
        $query->execute([ $Date,$BedandProcedures,$Maternity,$Xray,$Lab,$Theatre,$Mortuary,$OPTreatment,$Pharmacy,$id]);
        echo "<script> alert('Record edited successfully');window.open('healthdesigns/FacilityCollections.php?FacilityID=$FacilityID','_self')</script>";
    
    }
        public function editCommitmentSummary(){
        $id = $_POST['id'];    
        $itemcode = $_POST['itemcode'];
        $itemsubcode= $_POST['itemsubcode'];
        $itemdescription= $_POST['itemdescription'];
        $payments = $_POST['payments'];
        $cumulativepayments= $_POST['cumulativepayments'];
        $newcommitments= $_POST['newcommitments'];
        $outstandingcommitments= $_POST['outstandingcommitments'];
        $balance= $_POST['balance'];
        $date= $_POST['date'];
        $FacilityID=$_POST['FacilityID'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".paymentandcommitmentsummary SET  itemcode =?,itemsubcode =?, itemdescription =?,payments =?,cumulativepayments =?,newcommitments =?,outstandingcommitments =?,balance =? ,dateofsummary =?  ,approval_id =0 WHERE id= ?");
        $query->execute([ $itemcode,$itemsubcode,$itemdescription,$payments,$cumulativepayments,$newcommitments,$outstandingcommitments,$balance,$date,$id]);
        echo "<script> alert('Record edited successfully');window.open('healthdesigns/CommitmentSummary.php?FacilityID=$FacilityID','_self')</script>";
    
    }
         public function editBankReconciliation(){
        $id = $_POST['id'];    
        $date= $_POST['date'];
        $payee= $_POST['payee'];
        $amount= $_POST['amount'];
        $details= $_POST['details'];
        $FacilityID=$_POST['FacilityID'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".bankreconciliation SET  date =?,payee =?, amount =?,details =? ,approval_id =0 WHERE id= ?");
        $query->execute([ $date,$payee,$amount,$details,$id]);
        echo "<script> alert('Record edited successfully');window.open('healthdesigns/BankReconciliation.php?FacilityID=$FacilityID','_self')</script>";
    
    }
     public function editCashBook(){
        $id = $_POST['id'];    
        $bankstmtdate= $_POST['bankstmtdate'];
        $cashbookbalance = $_POST['cashbookbalance'];
        $totaldeposits= $_POST['totaldeposits'];
        $expenditures= $_POST['expenditures'];
        $bankcharges= $_POST['bankcharges'];
        $totalwithdrawals= $_POST['totalwithdrawals'];
        $bankstmtbalance= $_POST['bankstmtbalance'];
        $FacilityID=$_POST['FacilityID'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".cashbook SET  bankstmtdate =?,cashbookbalance =?, totaldeposits =?,expenditures =?,bankcharges =?,totalwithdrawals =?,bankstmtbalance =? ,approval_id =0 WHERE id= ?");
        $query->execute([ $bankstmtdate,$cashbookbalance,$totaldeposits,$expenditures,$bankcharges,$totalwithdrawals,$bankstmtbalance,$id]);
        echo "<script> alert('Record edited successfully');window.open('healthdesigns/CashBook.php?FacilityID=$FacilityID','_self')</script>";
    
    }
             public function declineApproval(){
              
        $recordID = $_POST['declineid'];
        $Comments = $_POST['Comments'];
    $query = $this->connect()->prepare("UPDATE ".$this->database.".facilitycollections SET approval_id= 2,Comments =? WHERE id= ?");
        $query->execute([ $Comments,$recordID]);
        echo "<script> alert('Record declined successfully');window.open('healthdesigns/FacilityCollectionsApproval.php?FacilityID=F1SO3M','_self')</script>";
    
    }
               public function Approve(){
                 
                 
        $recordID = $_POST['approveid'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".facilitycollections SET approval_id= 1 WHERE id= ?");
        $query->execute([ $recordID]);
        echo "<script> alert('Records Approved successfully');window.open('healthdesigns/FacilityCollectionsApproval.php?FacilityID=F1SO3M','_self')</script>";
  
    }
         public function ApproveCommitmentSummary(){
                 
                 
        $recordID = $_POST['approveid'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".paymentandcommitmentsummary SET approval_id= 1 WHERE id= ?");
        $query->execute([ $recordID]);
        echo "<script> alert('Records Approved successfully');window.open('healthdesigns/CommitmentSummaryApproval.php?FacilityID=F1SO3M','_self')</script>";
  
    }
      public function DeclineCommitmentSummary(){
                 
               
        $recordID = $_POST['declineid'];
        $Comments = $_POST['Comments'];   
    $query = $this->connect()->prepare("UPDATE ".$this->database.".paymentandcommitmentsummary SET approval_id= 2,Comments =? WHERE id= ?");
        $query->execute([  $Comments,$recordID]);
        echo "<script> alert('Records Declined successfully');window.open('healthdesigns/CommitmentSummaryApproval.php?FacilityID=F1SO3M','_self')</script>";
  
    }
    public function ApproveCashBook() {
                 
        $recordID = $_POST['approveid'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".cashbook SET approval_id= 1 WHERE id= ?");
        $query->execute([ $recordID]);
        echo "<script> alert('Records Approved successfully');window.open('healthdesigns/CashBookApproval.php?FacilityID=F1SO3M','_self')</script>";
  
        
    }
     public function DeclineCashBook(){
              
        $recordID = $_POST['declineid']; 
        $Comments = $_POST['Comments'];
    $query = $this->connect()->prepare("UPDATE ".$this->database.".cashbook SET approval_id= 2,Comments =? WHERE id= ?");
        $query->execute([$Comments,$recordID]);
        echo "<script> alert('Records Declined successfully');window.open('healthdesigns/CashBookApproval.php?FacilityID=F1SO3M','_self')</script>";
  
    }
      public function ApproveBankReconciliation() {
                 
        $recordID = $_POST['approveid'];

    $query = $this->connect()->prepare("UPDATE ".$this->database.".bankreconciliation SET approval_id= 1 WHERE id= ?");
        $query->execute([ $recordID]);
        echo "<script> alert('Records Approved successfully');window.open('healthdesigns/BankReconciliationApproval.php?FacilityID=F1SO3M','_self')</script>";
  
        
    }
     public function DeclineBankReconciliation(){
                 
               
        $recordID = $_POST['declineid'];
        $Comments = $_POST['Comments']; 
    $query = $this->connect()->prepare("UPDATE ".$this->database.".bankreconciliation SET approval_id= 2,Comments =? WHERE id= ?");
        $query->execute([$Comments,$recordID]);
        echo "<script> alert('Records Declined successfully');window.open('healthdesigns/BankReconciliationApproval.php?FacilityID=FacilityID=F1SO3M','_self')</script>";
  
    }

} $handler = new connection();

if(isset($_POST['FacilityCollections'])){
    $handler->addFacilityCollections();
}
if(isset($_POST['addFacilityCollections'])){
    $handler->addFacilityCollections();
}
if(isset($_POST['addcashbook'])){
    $handler->addcashbook();
}
if(isset($_POST['addbankreconciliation'])){
    $handler->addbankreconciliation();
}
if(isset($_POST['addpaymentsandcommitmentsummary'])){
    $handler->addpaymentsandcommitmentsummary();
}
if(isset($_POST['getAndDisplayUsers'])){
    $handler->getAndDisplayUsers();
}

if(isset($_POST['addFacility'])){
    $handler->addFacility();
}
if(isset($_POST['declineApproval'])){
    $handler->declineApproval();
}
if(isset($_POST['Approve'])){
    $handler->Approve();
}
if(isset($_POST['updateuserdetails'])){
    $handler->updateuserdetails();
}
if(isset($_POST['ApproveCommitmentSummary'])){
    $handler->ApproveCommitmentSummary();
}
if(isset($_POST['DeclineCommitmentSummary'])){
    $handler->DeclineCommitmentSummary();
}
if(isset($_POST['ApproveCashBook'])){
    $handler->ApproveCashBook();
}
if(isset($_POST['DeclineCashBook'])){
    $handler->DeclineCashBook();
}
if(isset($_POST['ApproveBankReconciliation'])){
    $handler->ApproveBankReconciliation();
}
if(isset($_POST['DeclineBankReconciliation'])){
    $handler->DeclineBankReconciliation();
}if(isset($_POST['editFacilityCollections'])){
    $handler->editFacilityCollections();
}
if(isset($_POST['editCommitmentSummary'])){
    $handler->editCommitmentSummary();
}
if(isset($_POST['editBankReconciliation'])){
    $handler->editBankReconciliation();
}
if(isset($_POST['editCashBook'])){
    $handler->editCashBook();
}
if(isset($_POST['addFacility'])){
    $handler->addFacility();
}
if(isset($_POST['getSystemUsers'])){
    $handler->getSystemUsers();
}
if(isset($_POST['deleteUser'])){
    $handler->deleteUser();
}
if(isset($_POST['editRankUser'])){
    $handler->editRankUser();
}
if(isset($_POST['assignUserFacility'])){
    $handler->assignUserFacility();
}
if(isset($_POST['sortFacilityCollectionsByDate'])){
    $handler->sortFacilityCollectionsByDate();
}
if(isset($_POST['getfacilities'])){
    $handler->getfacilities(); 
}
    if(isset($_POST['getFacilityCollectionsApprove()'])){
    $handler->getFacilityCollectionsApprove();
    
}if(isset($_POST['getuserdetails()'])){
    $handler->getuserdetails();
}

 

?>