
<?php
    @include_once "../utility.php";
    include_once '../Database.php';
    @include_once "utility.php";
    
    class Admin extends utility{
       
        function getrequests(){
            $rank = $_SESSION['rank'];
            echo "<center><h2>Records Approval</h2></center><br>";
            $result = $this->query("SELECT * FROM ","requests","WHERE status = ?",[$rank]);
            
            if($result->rowCount() == 0){
                echo "<center>No pending records Approval requests</center>";
            }else{
                echo "<table><tr>
                    <th>Date Requested</th>
                    <th>Facility Name</th>
                    <th>Records name</th>
                    </tr>";
                while($row = $result->fetch()){
                    echo "<tr>
                        <td>".$row['dateRequested']."</td>
                        <td>".$row['facilityName']."</td>
                        <td>".$row['recordName']."</td>
                        <td>
                            <form action='php/Admin.php' method='post'>
                                <input type='hidden' value='".$row['id']."' name='id'>
                                <input type='submit' value='Approve' name='approve' class='approve'>
                            </form>
                        </td>
                        <td>
                            <form action='../php/Admin.php' method='post'>
                                <input type='hidden' value='".$row['id']."' name='id'>
                                <input type='submit' value='Decline' name='decline' class='decline'>
                            </form>
                        </td>
                        </tr>";
                }
                echo "</table>";
            }
        }
        
        function approve(){
            $id = $_POST['id'];
            $rank = $_SESSION['rank'];
            if($rank == "MOH/PMO"){
                $result = $this->query("UPDATE ","requests","SET status=? WHERE id = ?",["ChiefOfficer", $id]);
            }else if($rank == "ChiefOfficer"){
                $result = $this->query("UPDATE ","requests","SET status=? WHERE id = ?",["UNASSIGNED", $id]);
            }
            
            header("Location:AdminRequests.php");
        }
        
        function decline(){
            $id = $_POST['id'];
            $rank = $_SESSION['rank'];
            if($rank == "MOH/PMO"){
                $result = $this->query("UPDATE ","requests","SET status=? WHERE id = ?",["DENIED MOH/PMO", $id]);
            }else if($rank == "ChiefOfficer"){
                $result = $this->query("UPDATE ","requests","SET status=? WHERE id = ?",["DENIED CO", $id]);
            }
           header("Location:AdminRequests.php");
            
        }        
        function approveRegistered(){
            $id = $_POST['id'];
            $rank = ($_POST['rank']);
            $res = $this->query("UPDATE","users","SET rank = ? WHERE IDNumber = ?",[$rank,$id]);
            header("Location:../healthdesigns/viewPendingReg.php");
        }
        
        function declineRegistered(){
            $id = $_POST['id'];
            $res = $this->query("DELETE FROM","users","WHERE ID = ?",[$id]);
            
            header("Location:../healthdesigns/viewPendingReg.php");
        }
         public function getfacilitiesAdmin(){
        $query = $this->query("select * from facilities");
        while($row=$query->fetch()){
            echo 
                "<option value=".$row['FacilityID'].">".$row['FacilityName']."</option>";
     
        }
    }
        function getRegisterPending(){
            $res = $this->query("SELECT * FROM","users","WHERE rank = ?",["PENDING"]);
            
         
            
            if($res->rowCount() == 0){
                echo "<center>No pending requests</center>";
            }else{
               
                $select = "<select id='rankVis' onchange='set()'>
                    <option>Facility Accountant</option>
                    <option>AIE Holder</option>
                    <option>MOH/PMO</option>
                </select>";
              
                            
                while($row=$res->fetch()){
                    echo "<tr>
                        <td>".$row['IDNumber']."</td>
                        <td>".$row['FirstName']."</td>
                        <td>".$row['LastName']."</td>
                        <td>".$row['JobID']."</td>
                        <td>$select</td>
                        <td>
                            <form method='post' action='../php/Admin.php' style='display: inline-block'>
                                <input type='hidden' name='rank' value='Facility Accountant' id='rank'>
                                <input type='hidden' name='id' value=".$row['IDNumber'].">
                                <input type='submit' value='Approve' name='approveReg' class='approve'>
                            </form>
                        </td>
                        <td>
                            <form method='post' action='php/Admin.php' style='display: inline-block'>
                                <input type='hidden' name='rank' value='Facility Accountant' id='rank'>
                                <input type='hidden' name='id' value=".$row['IDNumber'].">
                                <input type='submit' value='Decline' name='declineReg' class='decline'>
                            </form>
                        </td>
                        </tr>";
                }
            }
        }
        function getReport(){
            $res = $this->query("SELECT * FROM","records","WHERE status = ?",["APPROVED"]);
            $from = 0;
            $to = 0;
         
            if(isset($_POST['from'])){
                $from = str_replace("/","-",$_POST['from']);
            }
            
            if(isset($_POST['to'])){
                $to = str_replace("/","-",$_POST['to']);
            }
            
            if($from == 0 && $to == 0){
                $to = date('d-m-Y');
                $from = date('d-m-Y', strtotime('-1 months', strtotime($to)));
            }else if($from == 0){
                $from = $from = date('d-m-y', strtotime('-1 months', strtotime($to)));;
            }else if($to == 0){
                $to = $from = date('d-m-y', strtotime('+1 months', strtotime($to)));;
            }
            
            $records = array();
            $count = array();
             echo "<center><label class='label'>Showing usage from ".$from." to ".$to."</label><center>";
            while($row = $res->fetch()){
                $Date = $row['Date'];
                
                
                if(strtotime($Date) > (strtotime($to))){
                    break;
                }
                if(strtotime($Date) < strtotime($from)){
                    continue;
                }
                
                $used = explode(",",$row['request']);
                foreach($used as $mat){
                    $record_info = explode("-",$mat);
                    $found = 0;
                    for($i = 0; $i < count($materials); $i++){
                        if($records[$i] == $record_info[0]){
                            $found = 1;
                            $count[$i]+=$record_info[1];
                        }
                    }
                    if($found == 0){
                        array_push($records, $record_info[0]);
                        array_push($count, $record_info[1]);
                    }
                }
                                
            }
            
            $total = 0;
            $cost = array();
            
            for($i = 0; $i < count($records); $i++){
                $res = $this->query("SELECT * FROM","materials","WHERE name=?",[$materials[$i]]);
                $row = $res->fetch();
                $one = $row['price'];
                $t = $one * $count[$i];
                array_push($cost, $t);
            }
            
            
            echo "<table>
                    <tr>
                        <th>Facility Name</th>
                        <th>Records</th>
                        <th>Date</th>
                    </tr>";
            for($i = 0; $i < count($records); $i++){
                echo "<tr>
                        <td>".$RecordName[$i]."</td>
                        <td>".$TotalAmount[$i]."</td>
                        <td>".$cost[$i]."</td>
                    </tr>";
                $total+=$cost[$i];
            }
            echo "<tr><td>Account Statement</td><td></td><td>".$total."</td></tr>";
            echo "</table>";
            
        }
   
    }

    $Admin = new Admin();

    if(isset($_POST['approve'])){
        $Admin->approve();
    }

    if(isset($_POST['decline'])){
        $Admin->decline();
    }
if(isset($_POST['decline'])){
        $Admin->decline();
    }

    if(isset($_POST['approveReg'])){
        echo "here2";
        $Admin->approveRegistered();
    }

    if(isset($_POST['declineReg'])){
        $Admin->declineRegistered();
    }
?>
     