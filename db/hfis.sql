-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2020 at 08:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hfis`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankreconciliation`
--

CREATE TABLE `bankreconciliation` (
  `number` int(11) NOT NULL,
  `FacilityID` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `payee` tinytext NOT NULL,
  `amount` double NOT NULL,
  `details` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bankreconciliation`
--

INSERT INTO `bankreconciliation` (`number`, `FacilityID`, `date`, `payee`, `amount`, `details`) VALUES
(1, 'F1S03V', '2020-02-01', 'KCB NAKURU SALARIES', 2223600.6, ''),
(2, 'F1S03V', '2020-02-02', 'NHIF', 123400.8, ''),
(3, 'F1S03V', '2020-02-03', 'NSSF', 46000, ''),
(7, 'F1SO3M', '2020-03-05', 'NNSSH', 98765, 'paid in full amounts');

-- --------------------------------------------------------

--
-- Table structure for table `cashbook`
--

CREATE TABLE `cashbook` (
  `id` int(11) NOT NULL,
  `FacilityID` varchar(20) NOT NULL,
  `cashbookbalance` int(20) NOT NULL,
  `totaldeposits` int(20) NOT NULL,
  `otherdeposits` int(20) NOT NULL,
  `balanceanddeposit` int(20) NOT NULL,
  `expenditures` int(20) NOT NULL,
  `PMO_MOHcheque` int(20) NOT NULL,
  `otherpayments` int(20) NOT NULL,
  `bankcharges` int(20) NOT NULL,
  `totalwithdrawals` int(20) NOT NULL,
  `balanceforwaded` int(20) NOT NULL,
  `bankstmtdate` date NOT NULL,
  `bankstmtbalance` int(20) NOT NULL,
  `reconciliationattached` tinyint(1) NOT NULL,
  `CBuptodateasof` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cashbook`
--

INSERT INTO `cashbook` (`id`, `FacilityID`, `cashbookbalance`, `totaldeposits`, `otherdeposits`, `balanceanddeposit`, `expenditures`, `PMO_MOHcheque`, `otherpayments`, `bankcharges`, `totalwithdrawals`, `balanceforwaded`, `bankstmtdate`, `bankstmtbalance`, `reconciliationattached`, `CBuptodateasof`) VALUES
(1, 'F1SO3M', 8765, 34567, 87654, 234567, 987654, 78654, 345678, 987654, 345678, 98765, '2020-03-04', 987654, 127, '2020-03-06');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `FacilityID` varchar(20) NOT NULL,
  `FacilityName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`FacilityID`, `FacilityName`) VALUES
('F1SO3M', 'MedihealGroupOfHospitals'),
('F1N03N', 'NairobiWomenHospital'),
('F3N03N', 'NakuruLevel6Hospital'),
('F2N03N', 'NakuruSpecialistHospital'),
('F1S03V', 'ValleyHospital');

-- --------------------------------------------------------

--
-- Table structure for table `facilitycollections`
--

CREATE TABLE `facilitycollections` (
  `id` int(11) NOT NULL,
  `FacilityID` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `Date` date NOT NULL,
  `TSales` int(11) NOT NULL,
  `BedandProcedures` int(11) NOT NULL,
  `Maternity` int(11) NOT NULL,
  `Xray` int(45) NOT NULL,
  `Lab` int(45) NOT NULL,
  `Theatre` int(15) NOT NULL,
  `Mortuary` int(45) NOT NULL,
  `OPTreatment` int(50) NOT NULL,
  `Pharmacy` int(25) NOT NULL,
  `OtherB` int(50) NOT NULL,
  `CashDaily` int(11) NOT NULL,
  `CashNHIF` int(11) NOT NULL,
  `AmountB` int(11) NOT NULL,
  `Category` int(11) NOT NULL,
  `Amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilitycollections`
--

INSERT INTO `facilitycollections` (`id`, `FacilityID`, `Date`, `TSales`, `BedandProcedures`, `Maternity`, `Xray`, `Lab`, `Theatre`, `Mortuary`, `OPTreatment`, `Pharmacy`, `OtherB`, `CashDaily`, `CashNHIF`, `AmountB`, `Category`, `Amount`) VALUES
(10, 'F1SO3M', '2020-03-04', 4567, 897654, 87654, 345678, 87654, 987654, 234567, 98765, 345678, 87654, 3456, 98765, 98765, 435678, 98765),
(11, 'F1SO3M', '2020-03-06', 345, 987654, 324567, 987654, 98765, 34567, 987654, 9876543, 435689, 9087654, 345678, 987654, 324567, 98764, 9876543);

-- --------------------------------------------------------

--
-- Table structure for table `paymentandcommitmentsummary`
--

CREATE TABLE `paymentandcommitmentsummary` (
  `id` int(11) NOT NULL,
  `FacilityID` varchar(20) NOT NULL,
  `itemcode` int(11) NOT NULL,
  `itemsubcode` int(11) NOT NULL,
  `itemdescription` text NOT NULL,
  `newAIE` int(20) NOT NULL,
  `cumulativeAIE` int(20) NOT NULL,
  `payments` int(20) NOT NULL,
  `cumulativepayments` int(20) NOT NULL,
  `newcommitments` int(20) NOT NULL,
  `outstandingcommitments` int(20) NOT NULL,
  `balance` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paymentandcommitmentsummary`
--

INSERT INTO `paymentandcommitmentsummary` (`id`, `FacilityID`, `itemcode`, `itemsubcode`, `itemdescription`, `newAIE`, `cumulativeAIE`, `payments`, `cumulativepayments`, `newcommitments`, `outstandingcommitments`, `balance`) VALUES
(1, 'F1N03N', 456, 8976543, 'paid', 987654, 45678, 98765, 89765, 345678, 98765, 32456789);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `IDNumber` varchar(11) NOT NULL,
  `FirstName` varchar(20) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `JobID` int(20) NOT NULL,
  `EmailAddress` varchar(30) NOT NULL,
  `Password` varchar(80) NOT NULL,
  `rank` varchar(20) NOT NULL DEFAULT 'PENDING'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`IDNumber`, `FirstName`, `LastName`, `JobID`, `EmailAddress`, `Password`, `rank`) VALUES
('12121212', 'doris', 'nyawira', 2345678, 'nyawira@gmail.com', '$2y$10$ISHQGP.gJI0DkVzbtE9JDuGWFI.ETQWLaK1G0Z9z6i6xwIIHa2WEm', 'Chief Officer'),
('12341234', 'vic', 'dee', 45678, 'de@gmail.com', '$2y$10$YHLlj6N6w.7xcmFUukt.j.7nPyKv.K6hkxdGSba6cDjbit2ySKTE.', 'Accountant Officer'),
('1234567', 'januh', 'mbuvi', 1234, 'm@gamail.com', '$2y$10$kU/cB1UtX2Eax/rVizbGhOLEg6zDXq/Tiy9RRQNuoYmrcizBG5GvO', 'Accountant Officer'),
('12345678', 'fir', 'thowsa', 1234567, 'fur@gmail.com', '$2y$10$lfVykyKOjXihhyYvQzYeHOXcAwC1Gqo7l.kZLHNBFoXu2Vy9uZm9y', 'MOH/PMO'),
('12345679', 'brian', 'john', 12334567, 'b@gmail.com', '$2y$10$ws0ZpJRzQS/EfmYszB81/uuGYsmYDpGyiGWu2R3yqHWP5vgAK5u8a', 'Facility Accountant'),
('23232323', 'becky', 'wairimu', 234567, 'becky@gmail.com', '$2y$10$48YULOQExVgLgOH7gkm3COou3q97fKstwJyap1knce7yX8R4hHcMG', 'AIE Holder'),
('34343434', 'risper', 'bevalyn', 3245678, 'ris@gmail.com', '$2y$10$IVQh2WBsaKq5A5pi97pmcObSG2s5r/zB2hhljpGEuQbvxDeRxEoTq', 'AIE Holder'),
('44444444', 'brenda', 'nyaswa', 2345678, 'b@gmail.com', '$2y$10$keufbI/u0iEcjv.BNaWBTuuwE3.jSPg0SZ.lDmQ1tWnr/Q2Nuq1Ji', 'Facility Accountant'),
('45454545', 'firthowsa', 'sheikh', 345678, 'sheikh@gmail.com', '$2y$10$NaXwqJZr5DtmQxx6jKMO.uG73SIbBJBizsYWXuq48BtcvD1AodR6y', 'Accountant Officer'),
('56565656', 'ree', 'deee', 34567, 'rd@gmail.com', '$2y$10$sedTqoeVyLxI9/FbMzjMkO.F9gkWRvzV0prmxVGjrbqwVASFlwAX.', 'Other'),
('56789', 'name', 'last', 4567, '34567', '$2y$10$faNEsCs2ac9rQF7EcmYqtedFRYr7lW87SX6nLqSugEUqf3h8MWbqC', 'Other'),
('66666666', 'mercy', 'afandi', 9876543, 'mercy@gmail.com', '$2y$10$FHmu.MJZy4x9POmobR2/9Osr/o.MYuay91HXQEvxH5ap2xN0M/1wK', 'Facility Accountant'),
('88888888', 'doris', 'lenz', 0, 'dee@gmail.com', '$2y$10$.0JcTZiIXgFbyqA0Hjg7TOuQggOybKePc1eqJhd0OzbFRfhyepndC', 'Facility Accountant'),
('90909090', 'Godwin', 'Kibet', 987654, 'g@gmail.com', '$2y$10$nPnQ686Lr/j8DVuxl.3steyH/t.miceBVLxnfRfu5CKoSz/8AKBiS', 'Accountant Officer'),
('ewrtyui', 'iuytre', 'dsfghj', 0, 'ertyui', '$2y$10$RsJcEVFsWpMc7UoUoEtRnOCDPHKZUXvGF2QrQepxP/Ba6E5XLo/gi', 'PENDING'),
('ewrtyuio', 'dfhgjhj', 'oiuyt', 0, '456789', '$2y$10$C4Z.7t3s5NFQ9MqkGEVW1eYS7UkJe/bApT6673uXas6yYO26v8vYK', 'PENDING'),
('rt', 'liuiyty', 'ouiytr', 2147483647, 'liuytr', '$2y$10$cVaC2w8gdNvcqBbzblvwcOah3xyqWFcnMVAQM0yMWpP0eG9vHKP2y', 'PENDING'),
('rtyuio', 'rdtyfui', '98765', 0, 'oiuyt', '$2y$10$wXJw.w0xbOtA3B0Wmir/3OOEcas2K9GQU6HII1KkzYyuhi.Wieqha', 'PENDING');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankreconciliation`
--
ALTER TABLE `bankreconciliation`
  ADD PRIMARY KEY (`number`),
  ADD KEY `FacilityID` (`FacilityID`);

--
-- Indexes for table `cashbook`
--
ALTER TABLE `cashbook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FacilityID` (`FacilityID`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`FacilityID`),
  ADD UNIQUE KEY `FacilityName` (`FacilityName`);

--
-- Indexes for table `facilitycollections`
--
ALTER TABLE `facilitycollections`
  ADD PRIMARY KEY (`Date`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `FacilityID` (`FacilityID`);

--
-- Indexes for table `paymentandcommitmentsummary`
--
ALTER TABLE `paymentandcommitmentsummary`
  ADD PRIMARY KEY (`itemcode`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_FacilityID` (`FacilityID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`IDNumber`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankreconciliation`
--
ALTER TABLE `bankreconciliation`
  MODIFY `number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cashbook`
--
ALTER TABLE `cashbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `facilitycollections`
--
ALTER TABLE `facilitycollections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `paymentandcommitmentsummary`
--
ALTER TABLE `paymentandcommitmentsummary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bankreconciliation`
--
ALTER TABLE `bankreconciliation`
  ADD CONSTRAINT `bankreconciliation_ibfk_1` FOREIGN KEY (`FacilityID`) REFERENCES `facilities` (`FacilityID`);

--
-- Constraints for table `cashbook`
--
ALTER TABLE `cashbook`
  ADD CONSTRAINT `cashbook_ibfk_1` FOREIGN KEY (`FacilityID`) REFERENCES `facilities` (`FacilityID`);

--
-- Constraints for table `facilitycollections`
--
ALTER TABLE `facilitycollections`
  ADD CONSTRAINT `facilitycollections_ibfk_1` FOREIGN KEY (`FacilityID`) REFERENCES `facilities` (`FacilityID`);

--
-- Constraints for table `paymentandcommitmentsummary`
--
ALTER TABLE `paymentandcommitmentsummary`
  ADD CONSTRAINT `fk_FacilityID` FOREIGN KEY (`FacilityID`) REFERENCES `facilities` (`FacilityID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
