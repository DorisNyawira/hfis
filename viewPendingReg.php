<?php
session_start();
    require_once "../php/Admin.php";
    if(!$Admin->checklogin()){
        header("Location:healthdesigns/login.php");
    }else{
        if($_SESSION['rank'] != "MOH/PMO"){
            header("Location:healthdesigns/login.php");
        }
    }
    
    require_once "Database.php"; 
         
?>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin </title>

  <!-- Custom fonts for this template -->
  <link href="healthdesigns/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="healthdesigns/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="healthdesigns/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
 <script>
    
        function set(){
            var rank = document.getElementById('rank');
            var rankVis = document.getElementById('rankVis');
            rank.value = rankVis.value;
        }
    </script>
<body id="page-top">

  <!-- Page Wrapper -->
      
        <div style="margin-left: 0px;">
        <!--php $Admin->getRegisterPending();?>
    </div>-->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../index.html">
        <div class="sidebar-brand-icon rotate-n-15">
         
        </div>
          <div class="sidebar-brand-text mx-3"><b>HFIS</b></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
    

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>
  <!-- Nav Item - requests -->
      <li class="nav-item">
          <a class="nav-link" href="viewPendingReg.php">
          <span>Registration Requests</span></a>
      </li>
          <li class="nav-item">
              <a class="nav-link" href="healthdesigns/AdminReports.php?FacilityID=F1SO3M">
          <span> Report</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="addFacility.php">
          <span>Add Facility</span></a>
      </li>
           <!-- Divider -->
      <hr class="sidebar-divider">
        <li class="nav-item">
        <a class="nav-link collapsed" href="healthdesigns/viewPendingReg.php?FacilityID=F1SO3M" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          
          <span>Records Approval</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          
              <a class="collapse-item" href="healthdesigns/viewPendingReg.php?FacilityID=F1SO3M">Facility Collections</a>
              <a class="collapse-item" href="healthdesigns/CashBookApproval.php?FacilityID=F1SO3M">Cash book </a>
              <a class="collapse-item" href="healthdesigns/BankReconciliationApproval.php?FacilityID=F1SO3M">Bank Reconciliation</a>
              <a class="collapse-item" href="healthdesigns/CommitmentSummaryApproval.php?FacilityID=F1SO3M">Commitments Summary</a>
          </div>
        </div>
      </li>
       
      <li class="nav-item">
        <a class="nav-link collapsed" href="healthdesigns/FacilityCollectionsdeclined.php?FacilityID=F1SO3M" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
         
          <span>Declined  Records</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
         
              <a class="collapse-item" href="healthdesigns/FacilityCollectionsdeclined.php?FacilityID=F1SO3M">Facility Collections</a>
              <a class="collapse-item" href="healthdesigns/CashBookDeclined.php?FacilityID=F1SO3M">Cash Book</a>
              <a class="collapse-item" href="healthdesigns/BankReconciliationDeclined.php?FacilityID=F1SO3M">Bank Reconciliation</a>
              <a class="collapse-item" href="healthdesigns/CommitmentSummaryDeclined.php?FacilityID=F1SO3M">Commitments Summary</a>
          </div>
        </div>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Facility Records
      </div>

    
    
     

      <!-- Nav Item - Tables -->
    
        <li class="nav-item">
            <a class="nav-link" href="healthdesigns/FacilityCollectionsAdmin.php">
          <span>Facility Collections</span></a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="healthdesigns/paymentsandcommitmentsummaryAdmin.php">
          <span>Payments And Commitment Summary</span></a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="healthdesigns/CashBookAdmin.php">
          <span>Cash Book</span></a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="healthdesigns/BankReconciliationAdmin.php">
          <span>Bank Reconciliation</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <form class="form-inline">
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>
          </form>

          <!-- Topbar Search -->
          <a href="healthdesigns/Adminhelp.php"><button class="btn btn-primary pull-right" style="position:absolute;right:200px;bottom:20px;">
       <i class="fas fa-question-circle">Help</i>
             </button></a>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

        

            <div class="topbar-divider d-none d-sm-block"></div>

           <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Account Settings</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="healthdesigns/Adminuserprofile.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
            
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="fis/index.html" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>


          </ul>

        </nav>
      
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
    <div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
              <h6 class="m-0 font-weight-bold text-primary">Registration Requests</h6>
            </div> 
        <div class="table-responsive">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>
                           <tr>
                        <th>IDNumber</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>JobID</th>
                         <th>Rank</th>
                         
                         <th>Status Approve</th>
                          <th>Status Decline</th>
                        
                        </tr>
</thead>
<tbody>
   
        <?php $Admin->getRegisterPending();?>
  
</tbody>
</table>
</div>
          </div>
   
 
      <!-- End of Footer -->

  
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="fis/index.html">Logout</a>
        </div>
      </div>
    </div>
  </div> 
       
          <!-- DataTales Example -->
</div>
    
        </div>
        <!-- /.container-fluid -->

      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  
       <script>
            
        $('#declineModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var declineid=$(opener).attr('declineid');

//set what we got to our form
  $('#profileForm').find('[name="declineid"]').val(declineid);
   
});
            
        $('#approveModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var approveid=$(opener).attr('approveid');

//set what we got to our form
  $('#Approvalform').find('[name="approveid"]').val(approveid);
   
});
            
             </script>
</body>

</html>

