<html>
    <head>
<link rel="stylesheet" href="../style.css">
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <title>Facility Collections</title>
<link rel="stylesheet" href="style.css">
    <style>
.container{
    background-color:ghostwhite;
 width:100%;
 font-size: 20px;
}

</style>
</head>
<header>
<nav>
<div class="navbar">
<ul>
    <li class="current"><a href="addFacilityCollections.php">Facility Collections</a></li>
    <li><a href="addbankreconciliation.php">bank reconciliation</a></li>
    <li><a href="addcashbook.php">cash book</a></li>
    <li><a href="addpaymentsandcommitmentsummary.php">commitment summary</a></li>
    <li><a href="index.php">Log Out</a></li>
    <li><a href="help.php">Help</a></li>
    <li><a href="FacilityCollections.php">cancel</a></li>
  
    
</ul>
</div>
</nav>
</header>
<body>

<style>
    
    
        input[type="radio"]{
        
        }
        
            </style>
        
        
            <div>
            <form action="Database.php" method = "POST">
               
                <div class="container">
          <div class="row">
                
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    
                    <div class="col-lg-10 col-md-14 col-sm-14 col-xs-14">
                    <div class="jumbotron-fluid">
                        <h1 class="text-center">ADD FACILITY COLLECTIONS</h1>	
                             
                         
                                
                                        <div class="text-center" id="FacilityID">
                                            <h2> <label for="FacilityID">Facility Name</label></h2>
                                                <select id="FacilityID" name="FacilityID" text-center>
                                                     <option value="F1SO3M">Mediheal Group Of Hospitals</option>
                                                     <option value="F1N03N">Nairobi Women Hospital</option>
                                                     <option value="F3N03N">Nakuru Level 6 Hospital</option>
                                                     <option value="F2N03N">Nakuru Specialist Hospital</option>
                                                     <option value="F1S03V">Valley Hospital</option>
                                                </select><br><br><br>
                                              
                                            </div>
                                    
                               
                         <div class="row">
                             
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group" id="Date">
                                              <label class="control-table" for="Date">Date</label>
                                              <input type="date" name="Date" id="Date" class="form-control" placeholder="Enter the Date">
                                           
                                      </div>	
                                </div>
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                            <label class="control-table" for="TSales">Total Sales</label>
                                              <input type="text" name="TSales" id="TSales" class="form-control" placeholder="Enter Total Sales">
                                              
                                      </div>	
                                
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group" id="BedandProcedures">
                                              <label class="control-table" for="BedandProcedures">Bed And Procedures</label>
                                              <input type="text" name="BedandProcedures" id="BedandProcedures" class="form-control" placeholder="Enter Bed and Procedures Amount">
                                             
                                      </div>	
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Maternity">
                                                <label class="control-table" for="Maternity">Maternity</label>
                                                <input type="amount" name="Maternity" id="Maternity" class="form-control" placeholder="Enter Maternity Amount">
                                                
                                        </div>
                                    
                                </div>

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Xray">
                                                <label class="control-table" for="Xray">Xray</label>
                                                <input type="text" name="Xray" id="Xray" class="form-control" placeholder="Enter Xray Amount">
                                               
                                        </div>
                                    
                                </div>

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Lab">
                                                <label for="Lab">Lab</label>
                                                <input type="text" id="Lab" class="form-control" name="Lab"  placeholder="Enter the Lab Records" >
                                               
                                        </div>
                                    
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Theatre">
                                                <label for="Theatre">Theater</label>
                                                <input type="text" id="Theatre" class="form-control" name="Theatre" placeholder="Enter Theater Amount" >
                                                
                                        </div>
                            
                                </div>
                              
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Mortuary">
                                                <label for="Mortuary">Mortuary</label>
                                                <input type="text" name="Mortuary" id="Mortuary" class="form-control"  placeholder="Enter the Mortuary Records">
                                                   
                                             
                                            </div>
                                    
                                </div>  
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="OPTreatment">
                                                <label for="OPTreatment">OP Treatment</label>
                                                <input type="text" name="OPTreatment" id="OPTreatment" class="form-control"  placeholder="Enter the OPTreatment">
                                               
                                                
                                            </div>
                                    
                                </div>                                
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Pharmacy">
                                                <label for="Pharmacy">Pharmacy</label>
                                                <input type="text" name="Pharmacy" id="Pharmacy" class="form-control"  placeholder="Enter the Pharmacy Records">
                                                
                                            </div>
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="OtherB">
                                                <label for="OtherB">Other Balance</label>
                                                <input type="text" id="OtherB" name="OtherB" class="form-control"  placeholder="Enter Any Other Balance">
                                                   
                                            </div>
                        
                                </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="CashDaily">
                                                <label for="CashDaily">Daily Cash</label>
                                                <input type="text" id="CashDaily" name="CashDaily" class="form-control"  placeholder="Enter The Daily Cash">
                                                   
                                            </div>
                        
                                </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="CashNHIF">
                                                <label for="CashNHIF">NHIF Cash</label>
                                                <input type="text" id="CashNHIF" name="CashNHIF" class="form-control"  placeholder="Enter the NHIF Cash">
                                                   
                                            </div>
                        
                                </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="AmountB">
                                                <label for="AmountB">Amount Balance</label>
                                                <input type="text" id="OtherB" name="AmountB" class="form-control"  placeholder="Enter the Amount Balance">
                                                   
                                            </div>
                        
                                </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Category">
                                                <label for="Category">Category</label>
                                                <input type="text" id="Category" name="Category" class="form-control"  placeholder="Enter the Category">
                                                   
                                            </div>
                        
                                </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group" id="Amount">
                                                <label for="Amount">Amount</label>
                                                <input type="text" id="Amount" name="Amount" class="form-control"  placeholder="Enter the Total Amount">
                                                   
                                            </div>
                        
                                </div>
          
                            </div>
                    <div class="button">
                    <div style="align: center;" class="pull-right">
                    <input type="submit" class="btn btn-success" value="Submit" name="addFacilityCollections"></input>
                    <input type="submit" class="btn btn-success" value="cancel" ></input>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </form>
            </div>
        
            
        


            </body>
</html>