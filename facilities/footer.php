<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HOME</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" type="text/css"href="../style.css">
<style>
.showcase1{
 background-image: url(../img/new.jpg) ;
 background-size: cover;
}
.heading{
    background-image:url(../img/logo.jpg) ;
    background-repeat:no-repeat;
   
}
.showcase2{
    color:black;
    font-size:medium;
    background-color: white;
}
heading{
    top:3px;
    color:#3399ff;
    height:30px;
}
</style>
</head>
<div class="heading">
<h2><center>HEALTH FACILITY INFORMATION SYSTEM</center></h2>
</div>
<header>
    <nav>
        <ul>
 <li class="current"> <a href="index.php">Home</a></li>
 <li> <a href="login.php">Login</a></li>
 <li><a href="Register.php">Register</a></li>
<li> <a href="help.php">Help</a></li>
<li> <a href="facilities/footer.php">view</a></li>
</ul>
</nav>
</header>
<body>
   
 
    </section>
    <section class="showcase1">    
 </section>
<section class="showcase2">
 <div class="boxes" style="width:100%;">
     <div class="box" style="background-color:white;color:#99ccff;width:100%;">
        <p><b>HFIS PROVIDES A MORE SECURE,
            MORE TRANSPARENT AND MORE EFFICIENT PLATFORM.
          </b>   </p> 
 
<div class="container" style="height:100%; width:100%;">
<div class="card-deck" style="width:100%;">
<div class="card" style="width:33%; height:340px; top:50px;">
  <img class="card-img-top" src="../img/images.jpg" alt="Card image">
  <div class="card-body">
    <h4 class="card-title">Financial Discussion</h4>
    <p class="card-text">A group of people dicussing on facilities collections .</p>
   
  </div>
</div>

<div class="card" style="width:33%; height:340px; top:50px;">
    <img class="card-img-top" src="../img/financial-reporting-v2.png" alt="Card image">
</div>
    

<div class="card" style="width:33%; height:340px; top:50px;">
  <img class="card-img-top" src="../img/hfis.jpg" alt="Card image">
  <div class="card-body">
    <h4 class="card-title">Account Statements</h4>
    <p class="card-text">A List of statements on financial collections.</p>
  
  </div>
</div>
</div>
</div>
     </div>
 </div>
   </section> 
       <?php include_once "../footer.php";?>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  
</body>
</html>