   
<?php require_once 'FacilityAccountHeader.php'?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
 <div class="card shadow mb-4">
            <div class="card-header py-3">
        <script>
            function setName(){
                var selected = document.getElementById('facilityName').selectedIndex;
                localStorage.setItem("sel",selected);
            }
            
            function getName(){
                var selected = localStorage.getItem("sel");
                document.getElementById('facilityName').selectedIndex = selected;
            }
            getName();
        </script>
       
          

      <form action="../Database.php" method = "POST" onsubmit="return validatefacilitycollections()">
          
             <label style="margin-left:400px;"><b>FACILITY COLLECTIONS</b> </label><br><br>    
                
          <div class="row">
                
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    
                    <div class="col-lg-10 col-md-14 col-sm-14 col-xs-14">
                    <div class="jumbotron-fluid">
                        	

                         
                         <div class="row">
                             
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Date</label>
                                          <input type="hidden" name="FacilityID" value="<?php echo $_SESSION['FacilityID']?>" />
                                              <input type="date" name="Date" id="Date" class="form-control" placeholder="Enter the Date">
                                           
                                      </div>	
                                </div>
          
                               
                                
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Bed And Procedures</label>
                                              <input type="text" name="BedandProcedures" id="BedandProcedures" class="form-control" placeholder="Enter Bed and Procedures Amount">
                                             
                                      </div>	
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Maternity</label>
                                                <input type="amount" name="Maternity" id="Maternity" class="form-control" placeholder="Enter Maternity Amount">
                                                
                                        </div>
                                    
                                </div>

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Xray</label>
                                                <input type="text" name="Xray"id="Xray" class="form-control" placeholder="Enter Xray Amount">
                                               
                                        </div>
                                    
                                </div>

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Lab</label>
                                                <input type="text"  class="form-control" name="Lab" id="Lab"  placeholder="Enter the Lab Records" >
                                               
                                        </div>
                                    
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Theater</label>
                                                <input type="text" class="form-control" name="Theatre" id="Theatre" placeholder="Enter Theater Amount" >
                                                
                                        </div>
                            
                                </div>
                              
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Mortuary</label>
                                                <input type="text" name="Mortuary" id="Mortuary" class="form-control"  placeholder="Enter the Mortuary Records">
                                                   
                                             
                                            </div>
                                    
                                </div>  
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>OP Treatment</label>
                                                <input type="text" name="OPTreatment" id="OPTreatment" class="form-control"  placeholder="Enter the OPTreatment">
                                               
                                                
                                            </div>
                                    
                                </div>                                
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Pharmacy</label>
                                                <input type="text" name="Pharmacy" id="Pharmacy" class="form-control"  placeholder="Enter the Pharmacy Records">
                                                <input type="hidden" name="approval_id" class="form-control" value="0" >
                                                
                                            </div>
                                </div>
        
          
                            </div>
                    <div class="button">
                    <div style="align: center;" class="pull-right">
                    <input type="submit" class="btn btn-success" value="Submit" name="addFacilityCollections"></input>
                   
                    </div>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </form>

        </div>
        <!-- /.container-fluid -->

      </div>
        </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/validate.js"></script>
  <script src="js/alerts.js"></script>
</body>

</html>






