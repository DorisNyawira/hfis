<?php require_once 'adminheader.php'?>
                        
          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary"><b>COMMITMENT SUMMARY</b></label><br><br>
                
                <form class="form-inline" action="AdminCommitreport.php" method="GET" onsubmit="setName()">
                      <div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
 
  <div class="input-group">
    <div class="input-group-prepend" style="margin-left:20px;">
      <div class="input-group-text" id="btnGroupAddon">FROM</div>
    </div>
    <input type="date" class="form-control" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" name="fromDate" value="<?php if(isset($_GET['fromDate'])) { echo htmlentities ($_GET['fromDate']); }?>">
   <div class="input-group-prepend" style="margin-left:20px;">
      <div class="input-group-text" id="btnGroupAddon">TO</div>
    </div>
    <input type="date" class="form-control" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" name="toDate" value="<?php if(isset($_GET['toDate'])) { echo htmlentities ($_GET['toDate']); }?>">
      <input type="hidden" name="FacilityID" value="<?php echo $_SESSION['FacilityID']?>"/>
  
  </div>
    
</div> 
<div class="form-group mb-2" style="margin-left:10px">
  
    <select class="form-control" id="facilityName" name="FacilityID">
      <?php 
       $options = $handler->getfacilities(); 
       
                    @session_start();
                    if(isset($_SESSION['facilities'])){
                        $options = $_SESSION['facilities'];
                        unset($_SESSION['facilities']);
                    }
                    
                    echo $options;
                 ?>
                         </select>
   
           </div> 
<div class="form-group mx-sm-3 mb-2" style="margin-left:10px">
<button class="btn btn-primary mb-2" type="submit">Submit</button></div>
            </form> 
              <script>
                       var selected =""; 
            function setName(){
                var selected = document.getElementById('facilityName').selectedIndex;
                localStorage.setItem("sel",selected);
            }
            
            function getName(){
                var selected = localStorage.getItem("sel");
                document.getElementById('facilityName').selectedIndex = selected;
            }
            getName();
            document.getElementsByName('FacilityID')[0].onchange = function(e){
            document.getElementById('FacilityRecords').innerHTML = this.value;
                 };
            
        </script>
        </form>
            </div> 
          
        <div class="table-responsive">  
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>
	<tr>
		<th>Expense Item Code</th>
		<th>Expense Item Subcode</th>
                 <th>Expense Item Description</th>
                   <th >Payments</th>
                   <th>Cumulative Payments</th>
                  <th>New Commitments</th>
                   <th>Outstanding Commitments</th>
                   <th>Balance</th>
                    <th>Date</th>
                  
</tr>

</thead>
<tbody>
   <?php $handler->getpaymentsandcommitmentsummaryApprove();?>
</tbody>
</table>
</div>
          </div>
   
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
        
<script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>


  <!-- Page level custom scripts -->
<script >
      var title = "";
$(document).ready(function() {
    var v = $("#facilityName").val();
selected = $("#facilityName option[value="+v+"]").text()
    
   $('#dataTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                text: 'Print report',
                title: selected + " Commitment summary report",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            }
        ]
});
} );




    </script>
 
  
  <script src= https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js ></script>
<script src=https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js ></script>
<script src= https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js ></script>
<script src= https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js></script>
<script src =https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js ></script>
<script src=https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js ></script>

       <script>
            
        $('#declineModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var declineid=$(opener).attr('declineid');

//set what we got to our form
  $('#profileForm').find('[name="declineid"]').val(declineid);
   
});
            
             </script>
        

  
</body>

</html>










