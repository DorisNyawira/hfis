<?php require_once 'adminheader.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
            
            
<!-- Approve and decline modals -->

<!-- Modal -->
   
      
     
 
      <!-- End of Footer -->

  
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../fis/index.html">Logout</a>
        </div>
      </div>
    </div>
  </div> 
       
          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">Bank Reconciliation</label><br><br>
                <form class="form-inline" action="BankReconciliationDeclined.php" method="GET" onsubmit="setName()" >
                        
<div class="form-group mb-2">
  
    <select class="form-control" id="facilityName" name="FacilityID">
      <?php 
       $options = $handler->getfacilities(); 
       
                    @session_start();
                    if(isset($_SESSION['facilities'])){
                        $options = $_SESSION['facilities'];
                        unset($_SESSION['facilities']);
                    }
                    
                    echo $options;
                 ?>
                         </select>
   
           </div> 
<div class="form-group mx-sm-3 mb-2">
<button class="btn btn-primary mb-2" type="submit">Submit</button></div>
            </form> 
              <script>
            function setName(){
                var selected = document.getElementById('facilityName').selectedIndex;
                localStorage.setItem("sel",selected);
            }
            
            function getName(){
                var selected = localStorage.getItem("sel");
                document.getElementById('facilityName').selectedIndex = selected;
            }
            getName();
            document.getElementsByName('FacilityID')[0].onchange = function(e){
            document.getElementById('FacilityRecords').innerHTML = this.value;
                 };
            
        </script>
                   </div>   
        <div class="table-responsive">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
  
<thead>
	<tr>
	
   
   <th>date</th>
    <th>payee</th>
   <th>amount</th>
    <th>details</th>
   <th>Comments</th>
</tr>
     
   
</thead>
<tbody>
   <?php $handler->getBankReconciliationDecline();?>
</tbody>
</table>
</div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  
      
</body>

</html>







