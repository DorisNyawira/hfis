 
<?php require_once 'FacilityAccountHeader.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
   <form id="profileForm" action="../Database.php" method="POST">
 
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
   <div class="modal-body">
  <div class="form-group mb-2">
      <h6>date</h6>
  </div>
   <div class="form-group mx-sm-3 mb-2">
   <input type="hidden" class="form-control" id="id" name="id" value="">
    <input type="date" class="form-control" id="date" name="date" placeholder="date"value="">
  </div>
  <div class="form-group mb-2">
      <h6>Beds & Procedures</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="BedandProcedures"name="BedandProcedures" placeholder="BedandProcedures"value="">
  </div>
             <div class="form-group mb-2">
      <h6>Maternity</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="Maternity"name="Maternity" placeholder="Maternity"value="">
  </div>
               
  <div class="form-group mb-2">
      <h6>Xray</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="Xray"name="Xray" placeholder="Xray"value="">
  </div>
   <div class="form-group mb-2">
      <h6>Lab</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="Lab"name="Lab" placeholder="Lab"value="">
  </div>
   <div class="form-group mb-2">
      <h6>Theatre</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="Theatre"name="Theatre" placeholder="Theatre"value="">
  </div>
  <div class="form-group mb-2">
      <h6>Mortuary</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="Mortuary"name="Mortuary" placeholder="Mortuary"value="">
  </div>
  <div class="form-group mb-2">
      <h6>OP Treatment</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="OPTreatment"name="OPTreatment" placeholder="OPTreatment"value="">
  </div>
  <div class="form-group mb-2">
      <h6>Pharmacy</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="Pharmacy"name="Pharmacy" placeholder="Pharmacy"value="">
  </div>
 
           </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="hidden" name="FacilityID" value="<?php if(isset($_SESSION['FacilityID'])) { echo htmlentities ($_SESSION['FacilityID']); }?>">
         <button type="submit" class="btn btn-success"  name="editFacilityCollections" value="editFacilityCollections">Edit</button>
      </div>
    
    </div>
  </div>
</div>
      </form>
       
          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
                 <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">Facility Collections</label>
                <form class="form-inline" action="FacilityCollectionsdeclinedfa.php" method="GET" onsubmit="setName()">

            </form> 
            
            </div> 
        <div class="table-responsive">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>

<tr>
	
   <th>Date</th>
    <th>Beds & Procedures</th>
   <th>Maternity</th>
   <th>Xray</th>
   <th>Lab</th>
   <th>Theatre</th>
   <th>Mortuary</th>
   <th>OP treatment</th>
   <th>Pharmacy</th>
  <th>Comments</th>
    <th>Edit</th>
  
  
   
</tr>


   
</thead>
<tbody>
   <?php $handler->getFacilityCollectionsDeclinefa();?>
</tbody>
</table>
</div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
     <script>
       $('#editModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
     var id=$(opener).attr('id');
  $('#profileForm').find('[name="id"]').val(id);
  var date=$(opener).attr('date');
  $('#profileForm').find('[name="date"]').val(date);
    var BedandProcedures=$(opener).attr('BedandProcedures');
  $('#profileForm').find('[name="BedandProcedures"]').val(BedandProcedures);
   var Maternity=$(opener).attr('Maternity');
  $('#profileForm').find('[name="Maternity"]').val(Maternity);
    var BedandProcedures=$(opener).attr('BedandProcedures');
  $('#profileForm').find('[name="BedandProcedures"]').val(BedandProcedures);
   var Xray=$(opener).attr('Xray');
  $('#profileForm').find('[name="Xray"]').val(Xray);
    var Lab=$(opener).attr('Lab');
  $('#profileForm').find('[name="Lab"]').val(Lab);
   var Theatre=$(opener).attr('Theatre');
  $('#profileForm').find('[name="Theatre"]').val(Theatre);
    var Mortuary=$(opener).attr('Mortuary');
  $('#profileForm').find('[name="Mortuary"]').val(Mortuary);
    var OPTreatment=$(opener).attr('OPTreatment');
  $('#profileForm').find('[name="OPTreatment"]').val(OPTreatment);
    var Pharmacy=$(opener).attr('Pharmacy');
  $('#profileForm').find('[name="Pharmacy"]').val(Pharmacy);
   
});
     </script>
     

</html>





