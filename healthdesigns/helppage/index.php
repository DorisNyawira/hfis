<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>document.getElementsByTagName("html")[0].className += " js";</script>
  <link rel="stylesheet" href="assets/css/style.css">
  <title>help page</title>
</head>
<body style="background-color:white;">
<header class=" flex flex-column flex-center" style="background-color:#008eff;height:60px;">
    <h5 style="color:white;">HELP PAGE </h5>
 <div class="navbar navbar-default">
 <div style="position: absolute;right:20px;top:10px;color:white;">
     <a href="../../fis/index.html" style="color:white;"><button type="button" class="btn btn-outline-primary navbar-btn">Home</button></a>
     <a href="../login.php" style="color:white;"><button type="button" class="btn btn-outline-info">Log in</button></a>
     <a href="../Register.php" style="color:white;"> <button type="button" class="btn btn-outline-light">Sign up</button></a>
     
         
 </div> 
  
</div>
</header>

<section class="cd-faq js-cd-faq container max-width-md margin-top-lg margin-bottom-lg">
	<ul class="cd-faq__categories">
		<li><a class="cd-faq__category cd-faq__category-selected truncate" href="#basics">Basics</a></li>
		<li><a class="cd-faq__category truncate" href="#account">Account</a></li>
		<li><a class="cd-faq__category truncate" href="#privacy">Privacy</a></li>
		
	</ul> <!-- cd-faq__categories -->

	<div class="cd-faq__items">
		<ul id="basics" class="cd-faq__group">
			<li class="cd-faq__title"><h2>Basics</h2></li>
			<li class="cd-faq__item" >
				<a class="cd-faq__trigger" href="#0" style="color:#0040ff"><span>How do I change my password?</span></a>
				<div class="cd-faq__content" >
          <div class="text-component">
            <p>click on the login button to login to your account.on the top right of the page ,
                                        you will find "account Settings" click on the button, it will prompt you with edit profile screen.click on change password to enter your new password.</p>
          </div>
				</div> <!-- cd-faq__content -->
			</li>

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0"style="color:#0040ff"><span>How do I sign up?</span></a>
				<div class="cd-faq__content">
                     <div class="text-component">
                         
                         <p> click on the sign up button on the top right corner of the home page. It will prompt you with a sign up page.
                         Enter your details and click on the sign up button to submit.</p>
                     </div>
				</div> <!-- cd-faq__content -->
			</li>

			

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0"style="color:#0040ff"><span>How do  I log in?</span></a>
				<div class="cd-faq__content">
                                    <div class="text-component">
                                        <p> Click on the login button on the top right corner.it will prompt you with a login screen where you will enter the details.
                                        Incase your rank hasn't been approved by the Ministry of Health(MOH) or Project Manager Officer(PMO),you will have to wait and try to login later. </p> 
                                        
                                    </div>
            		</div> <!-- cd-faq__content -->
			</li>
		</ul> <!-- cd-faq__group -->

		
		</ul> <!-- cd-faq__group -->

		<ul id="account" class="cd-faq__group">
			<li class="cd-faq__title"><h2>Account</h2></li>
			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0" style="color:#0040ff"><span>How do I change my password?</span></a>
				<div class="cd-faq__content">
                                    <div class="text-component">
                                        <p>click on the login button to login to your account.on the top right of the page ,
                                        you will find "account Settings" click on the button, it will prompt you with edit profile screen.click on change password to enter your new password.</p>
                                    </div>
           
				</div> <!-- cd-faq__content -->
			</li>

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0"style="color:#0040ff"><span>How do I delete my account?</span></a>
				<div class="cd-faq__content">
          <div class="text-component">
            click on the login button to login to your account.on the top right of the page ,
                                        you will find "account Settings" click on the button, it will prompt you with edit profile screen.
                                        click on delete button to delete your account.
				</div> <!-- cd-faq__content -->
			</li>

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0"style="color:#0040ff"><span>How do I change my account settings?</span></a>
				<div class="cd-faq__content">
          <div class="text-component">
              <p> click on the login button to login to your account.on the top right of the page ,
                                        you will find "account Settings" click on the button, it will prompt you with edit profile screen.
                                        Edit your profile according to your preference.
              </p>
          </div>
				</div> <!-- cd-faq__content -->
			</li>

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0"style="color:#0040ff"><span>I forgot my password. How do I reset it?</span></a>
				<div class="cd-faq__content">
          <div class="text-component">
            <p> click on the login button it will take you to the login page . click the forgot password button 
            it will prompt you with a screen where you will provide your email address.login to your email and click on the link to reset your password.
            Enter your new password and use it while you login in.</p>
          </div>
                                    
				</div> <!-- cd-faq__content -->
			</li>
		</ul> <!-- cd-faq__group -->

	
		<ul id="privacy" class="cd-faq__group">
			<li class="cd-faq__title"><h2>Privacy</h2></li>
			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0" style="color:#0040ff"><span>Can I specify my own private key?</span></a>
				<div class="cd-faq__content">
          <div class="text-component">
              <p> when you sign up you will provide your own ID number and Job ID that will be unique to others.
              You will use your ID Number to sign up to your account .Nobody else will have access to it</p>		
          </div>
          </div> <!-- cd-faq__content -->
			</li>

		

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0" style="color:#0040ff"><span>How can I access my account data?</span></a>
				<div class="cd-faq__content">
          <div class="text-component">
              <p>Provide your login details to login to your account , there you can access your details. </p>
          </div>
				</div> <!-- cd-faq__content -->
			</li>

			<li class="cd-faq__item">
				<a class="cd-faq__trigger" href="#0" style="color:#0040ff"><span>How can I control if other search engines can link to my profile?</span></a>
				<div class="cd-faq__content">
          <div class="text-component">
              <p>When you login to your page you will be provided with information that you can only access that others don't have access to. </p>
          </div>
				</div> <!-- cd-faq__content -->
			</li>
		</ul> <!-- cd-faq__group -->

	</div> <!-- cd-faq__items -->

	<a href="#0" class="cd-faq__close-panel text-replace">Close</a>
  
  <div class="cd-faq__overlay" aria-hidden="true"></div>
</section> <!-- cd-faq -->
  <div class="footer-area white" style="background-color:#008eff;height:100px;">
        <div class="footer-top-area blue-bg padding-100-50">
            <div class="container">
                <div class="row">
               
            </div>
        </div>
        <div class="footer-bottom-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-social-bookmark text-center wow fadeIn">
                             <h5> HFIS </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved 
</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
<script src="assets/js/util.js"></script> <!-- util functions included in the CodyHouse framework -->
<script src="assets/js/main.js"></script> 
</body>
 
</html>
