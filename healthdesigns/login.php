<?php
    require_once "../utility.php";
    $utility = new utility();
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
 
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>HFIS</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
</head>
<header>
     <?php require_once"../utility.php";
            $utility->report();
        ?>
 
  </header>
<body>
 <form action='../utility.php' method='post' style="background-color:#3399ff;">
  <div class="container">
 <!-- Outer Row -->
    <div class="row justify-content-center">
   <div class="col-xl-5 col-lg-4 col-md-8 col-xs-2">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row" style="font-size: 22px;">
       
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <form class="user">
                  
                      <div class="form-group"><label class="small mb-1" for="inputidNumber"><b>ID Number</b></label><input class="form-control py-4" id="id" name="id"type="text" placeholder="Enter ID Number" /></div>
                      <div class="form-group"><label class="small mb-1" for="inputPassword"><b>Password</b></label><input class="form-control py-4" id="pass" name="pass" type="password" placeholder="Enter password" /></div>
         
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                        </div>
                        <div class="float-right">
                        <a> <input type="submit" name='login' value="login" style="background-color:#3399ff;width:100px;color:white;"></a>
                      </div>
                    </div>
                  </form><br>
                  <hr>
                  <div class="text-center">
                      <a class="small" href="forgotpassword.php">Forgot Password?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="Register.php">Create an Account!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
   <script type="text/javascript" src="../hide.js"></script>
  <script type=text/javascript src='../validation.js'></script> 
 </form>
</body>
<?php  include_once 'footer.php'; ?>
</html>
