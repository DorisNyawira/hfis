<?php require_once 'adminheader.php'?>



<div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecordss" class="m-0 font-weight-bold text-primary"> Assign Facilities</label>
                <form class="form-inline" action="../Database.php" method="POST">
                
<div class="form-group mb-2">
  
    <select class="form-control" id="facilityNames" name="FacilityIDs">
      <?php 
       $handler->getfacilities(); 
       
                    
                 ?>
                         </select>
   
           </div> 
                    <div class="form-group mb-2" style="margin-left:30px">
  
    <select class="form-control" id="UserID" name="UserIDNumber">
      <?php 
       $handler->getSystemUsers(); 
                 ?>
                         </select>
   
           </div> 
<div class="form-group mx-sm-3 mb-2">
<button class="btn btn-primary mb-2" type="submit" name="assignUserFacility">Submit</button></div>
            </form> 

            </div> 

</div>



          <div class="card shadow mb-4" >
     <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">Facility Records</label>
                <form class="form-inline" action="assignFacilities.php" method="GET" onsubmit="setName()">
                      
<div class="form-group mb-2">
  
    <select class="form-control" id="facilityName" name="FacilityID">
      <?php 
       $options = $handler->getfacilities(); 
       
                    @session_start();
                    if(isset($_SESSION['facilities'])){
                        $options = $_SESSION['facilities'];
                        unset($_SESSION['facilities']);
                    }
                    
                    echo $options;
                 ?>
                         </select>
   
           </div> 
<div class="form-group mx-sm-3 mb-2">
<button class="btn btn-primary mb-2" type="submit">Submit</button></div>
            </form> 
          
            </div> 
           
          
        <div class="table-responsive">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>


                        <tr>
                        <th>EmailAddress</th>
                        <th>Job ID</th>
                        <th>Rank</th>
                        <th>Action</th>
                        </tr>
  
  
  
  
   



   
</thead>
<tbody>
   <?php $handler->getAndDisplayUsers();?>
</tbody>
</table>
</div>

      

            
    <form method="post" action="../Database.php">       
  <!-- Logout Modal-->
  <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete the user!!</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "delete" below if you are ready to permantelly delete the user</div>
          <input type="text" name="userID" value=""/>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-danger" type="submit" name="deleteUser">Delete</button>
         
        </div>
          
      </div>
    </div>
  </div> 
              </form>
                          
    <form method="post" action="../Database.php">       

      <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><label for='useremail'> </label></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
         <div class="row">
      <div class="col-md-4">Edit facility</div>
              <input type="hidden" name="userID" value=""/>
      <div class="col-md-4">
             <select class="browser-default custom-select" style="width:250px" id="facilityName" name="FacilityID">
                 <option selected>Open this select menu</option>
      <?php 
       $handler->getfacilities(); 
                 ?>
                         </select>
             </div>
             
    </div>
                   <div class="row" style="margin-top:10px">
      <div class="col-md-4">Edit role</div>
      <div class="col-md-4">
            
<select class="browser-default custom-select" style="width:250px" name="rank">
  <option selected>Open this select menu</option>
  <option value="Facility Accountant">Facility Accountant</option>
  <option value="MOH/PMO">MOH/PMO</option>
  <option value="AIE Holder">AIE Holder</option>
          </select>
    </div>

          </div>
         
        <div class="modal-footer" style="margin-top:10px">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-success" type="submit" name="editRankUser">Edit</button>
         
        </div>
          
      </div>
    </div>
  </div> 
     

    </div>
            
</div>
              </form>

</div>





<!--Have the edit and delete modal here.   -->



<footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
        
     
  
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
 
  
  <script src= https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js ></script>
<script src=https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js ></script>
<script src= https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js ></script>
<script src= https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js></script>
<script src=https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js></script>
<script src =https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js ></script>
<script src=https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js ></script>

       <script>
            
        $('#deleteUser').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var deleteUserID=$(opener).attr('id');

//set what we got to our form
  $('#deleteUser').find('[name="userID"]').val(deleteUserID);
   
});
    $('#editUser').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
  
        var editUserID=$(opener).attr('id');

//set what we got to our form
  $('#editUser').find('[name="userID"]').val(editUserID);
   //we get details from attributes
  var editUserEmail=$(opener).attr('email');

//set what we got to our form
  $("label[for='useremail']").text('Edit'+' '+editUserEmail +' '+'role and rank');
   
});    
             </script>
        
       <script>
            function setName(){
                var selected = document.getElementById('facilityName').selectedIndex;
                localStorage.setItem("sels",selected);
            }
            
            function getName(){
                var selected = localStorage.getItem("sels");
                document.getElementById('facilityName').selectedIndex = selected;
            }
            getName();
            document.getElementsByName('FacilityID')[0].onchange = function(e){
            document.getElementById('FacilityRecords').innerHTML = this.value;
                 };
            
        </script>
  
</body>

</html>
