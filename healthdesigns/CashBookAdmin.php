<?php require_once 'adminheader.php'?>
      <!-- DataTales Example -->
          <div class="card shadow mb-4" >
               <div class="card-header py-3" style="margin-bottom: 10px;">
                   <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">Cash Book </label><br><br>
                   
                <form class="form-inline" action="CashBookAdmin.php" method="GET" onsubmit="setName()" >
   <div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
 
  <div class="input-group">
    <div class="input-group-prepend" style="margin-left:20px;">
      <div class="input-group-text" id="btnGroupAddon">FROM</div>
    </div>
    <input type="date" class="form-control" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" name="fromDate" value="<?php if(isset($_GET['fromDate'])) { echo htmlentities ($_GET['fromDate']); }?>">
   <div class="input-group-prepend" style="margin-left:20px;">
      <div class="input-group-text" id="btnGroupAddon">TO</div>
    </div>
    <input type="date" class="form-control" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" name="toDate" value="<?php if(isset($_GET['toDate'])) { echo htmlentities ($_GET['toDate']); }?>">
      <input type="hidden" name="FacilityID" value="<?php echo $_SESSION['FacilityID']?>"/>
  
  </div>
    
</div> 
<div class="form-group mb-2" style="margin-left:10px">
  
    <select class="form-control" id="facilityName" name="FacilityID">
      <?php 
       $options = $handler->getfacilities(); 
       
                    @session_start();
                    if(isset($_SESSION['facilities'])){
                        $options = $_SESSION['facilities'];
                        unset($_SESSION['facilities']);
                    }
                    
                    echo $options;
                 ?>
                         </select>
   
           </div> 
<div class="form-group mx-sm-3 mb-2" style="margin-left:10px">
<button class="btn btn-primary mb-2" type="submit">Submit</button></div>
            </form> 
              <script>
            function setName(){
                var selected = document.getElementById('facilityName').selectedIndex;
                localStorage.setItem("sel",selected);
            }
            
            function getName(){
                var selected = localStorage.getItem("sel");
                document.getElementById('facilityName').selectedIndex = selected;
            }
            getName();
            document.getElementsByName('FacilityID')[0].onchange = function(e){
            document.getElementById('FacilityRecords').innerHTML = this.value;
                 };
            
        </script>
                   </div>  
        <div class="table-responsive">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>
	
<tr>
     <th>bank stmt date</th>
    <th>cash book balance</th>
   <th>total deposits</th>
   <th>expenditures</th>
   <th>PMO/MOH cheque</th>
   <th>total withdrawals</th>
  <th>bank stmt balance</th>

</tr>
</thead>
<tbody>
    <?php $handler->getcashbookApprove(); ?>
</tbody>


</table>
</div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  
      
</body>

</html>








