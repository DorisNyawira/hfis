 <?php require_once 'AIHeader.php'?>

        <!-- Begin Page Content -->

        <div class="container-fluid">
     
 <div class="card shadow mb-4">
            <div class="card-header py-3">
        
       
          

      <form action="../Database.php" method = "POST" onsubmit="return validatecommitmentsummary()">
               
                
     <div class="row">
                
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    
                    <div class="col-lg-10 col-md-14 col-sm-14 col-xs-14">
                    <div class="jumbotron-fluid">
      
                             
                         </div>
                               
                         <div class="row">
                             
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Item Code</label>
                                              <input type="text" name="itemcode"  id="itemcode" class="form-control" placeholder="Enter the item code">
                                             
                                      </div>	
                                </div>
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                    <label>Item Sub Code</label>
                                              <input type="text" name="itemsubcode" id="itemsubcode" class="form-control" placeholder="Enter the item subcode">
                                              
                                      </div>	
                                
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Item Description</label>
                                              <input type="text" name="itemdescription" class="form-control" id="itemdescription" placeholder="Enter the item description">
                                             
                                      </div>	
                                </div>
        
                            

                              

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Payments</label>
                                                <input type="text"  class="form-control" name="payments" id="payments"  placeholder="Enter the payments" >
                                               
                                        </div>
                                    
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Cumulative Payments</label>
                                                <input type="text" class="form-control" name="cumulativepayments" id="cumulativepayments" placeholder="Enter the cumulative payments" >
                                                
                                        </div>
                            
                                </div>
                              
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>New Commitments</label>
                                                <input type="text" name="newcommitments" id="newcommitments" class="form-control"  placeholder="Enter the New Commitments">
                                                   
                                             
                                            </div>
                                    
                                </div>  
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Outstanding Commitments</label>
                                                <input type="text" name="outstandingcommitments" id="outstandingcommitments" class="form-control"  placeholder="Enter the Outstanding Commitments ">
                                               
                                                
                                            </div>
                                    
                                </div>                                
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Balance</label>
                                                <input type="text" name="balance" id="balance" class="form-control"  placeholder="Enter the balance">
                                                 <input type="hidden" name="approval_id" class="form-control" value="0" >
                                            <input type="hidden" value="<?php if(!empty($_SESSION['FacilityID'])){
    echo $_SESSION['FacilityID'];} else{echo 0;} ?>" name="FacilityID" />
                                            </div>
                                </div>
                              <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Date</label>
                                                <input type="date" name="date" id="date" class="form-control"  placeholder="Enter the date">
                                            </div>
                                </div>
        
                                
                        
                                </div>
          <div class="button">
                    <div style="align: center;" class="pull-right">
                    <input type="submit" class="btn btn-success" value="Submit" name="addpaymentsandcommitmentsummary"></input>
                   
                    </div>
                    </div>
                            </div>
                    
                    </div>
                    
                </div>
                  
        </form>

        </div>
        <!-- /.container-fluid -->

      </div>
        </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
   <script src="js/validate.js"></script>
  <script src="js/alerts.js"></script>
</body>

</html>






