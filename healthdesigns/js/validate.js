
function validateRegister() {
        var id = document.getElementById('id').value; 
        var fname = document.getElementById('fname').value;
	var lname = document.getElementById('lname').value;
	var jobid = document.getElementById('jobid').value; 
        var email = document.getElementById('email').value;
        var pass = document.getElementById('pass').value;
	var confirm = document.getElementById('cpass').value;
	var mess = document.getElementById('message');
	var mess2 = document.getElementById('message2');

	if ( id.length == 0 || fname.length == 0 || lname.length == 0 || 
			 jobid.length == 0 || email.length == 0 ||
			 pass.length == 0 || confirm.length == 0) {
		swal({
			  title: "Some Fields Empty!",
			  text: "Fill all fields !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
          var passRegEx=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
	if (pass.length < 8 || !pass.match(passRegEx)) {
		swal({
			  title: "Password invalid!",
			  text: "Password should be at least 8 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}

	if (pass !== confirm) {
		swal({
			  title: "Password Dont match!",
			  text: "Password and password confirmation should match!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}

	var idRegEx = /^[0-9]+$/;

	if (id.length < 7 || id.length > 8 || !id.match(idRegEx)) {
		swal({
			  title: "Id number invalid!",
			  text: "Enter a valid id number!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
           var fnameidRegEx =/^[A-Za-z\'\s\.\,]+$/;           
	if (fname.length < 3 || !fname.match(fnameidRegEx)) {
		swal({
			  title: "First name invalid!",
			  text: "Enter a valid first name!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}

             var lnameidRegEx =/^[A-Za-z\'\s\.\,]+$/; 
	if (lname.length < 3 || !lname.match(lnameidRegEx)) {
		swal({
			  title: "Last name invalid!",
			  text: "Enter a valid last name!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        	var jobidRegEx = /^(?=.*[a-zA-Z])(?=.*[0-9])/;
	if (jobid.length < 7 || !jobid.match(jobidRegEx)) {
		swal({
			  title: "job id invalid!",
			  text: "Enter a valid job id!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        var emailRegEx=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!email.match(emailRegEx)) {
		swal({
			  title: "Email invalid!",
			  text: "Enter a valid Email Address!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}

	mess.innerHTML = "";
	mess2.innerHTML = "registered awaiting approval";
	return true;
}
function validatecashbook(){
        var bankstmtdate = document.getElementById('bankstmtdate').value; 
        var cashbookbalance = document.getElementById('cashbookbalance').value;
	var totaldeposits = document.getElementById('totaldeposits').value;
	var expenditures = document.getElementById('expenditures').value; 
        var bankcharges = document.getElementById('bankcharges').value;
        var totalwithdrawals = document.getElementById('totalwithdrawals').value;
	var bankstmtbalance = document.getElementById('bankstmtbalance').value;
	var mess = document.getElementById('message');
	var mess2 = document.getElementById('message2');

	if ( bankstmtdate.length == 0 || cashbookbalance.length == 0 || totaldeposits.length == 0 || 
			 expenditures.length == 0 || bankcharges.length == 0 ||
			 totalwithdrawals.length == 0 || bankstmtbalance.length == 0) {
		swal({
			  title: "Some Fields Empty!",
			  text: "Fill all fields !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
  
          var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !cashbookbalance.match(amountRegEx)) {
		swal({
			  title: "cash book balance entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
              var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !totaldeposits.match(amountRegEx)) {
		swal({
			  title: "Total Deposits entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
               var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !expenditures.match(amountRegEx)) {
		swal({
			  title: "Expenditures entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
                  var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !bankcharges.match(amountRegEx)) {
		swal({
			  title: "Bank charges entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
                    var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !totalwithdrawals.match(amountRegEx)) {
		swal({
			  title: "total withdrawals entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
               var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !bankstmtbalance.match(amountRegEx)) {
		swal({
			  title: "bank statement balance entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        mess.innerHTML = "";
	mess2.innerHTML = "";
	return true;
}
function validatecommitmentsummary(){
        var itemcode = document.getElementById('itemcode').value; 
        var itemsubcode = document.getElementById('itemsubcode').value;
	var itemdescription = document.getElementById('itemdescription').value;
	var payments = document.getElementById('payments').value; 
        var cumulativepayments = document.getElementById('cumulativepayments').value;
        var newcommitments = document.getElementById('newcommitments').value;
	var outstandingcommitments = document.getElementById('outstandingcommitments').value;
        var balance = document.getElementById('balance').value;
	var mess = document.getElementById('message');
	var mess2 = document.getElementById('message2');

	if ( itemcode.length == 0 || itemsubcode.length == 0 || itemdescription.length == 0 || 
			 payments.length == 0 || cumulativepayments.length == 0 ||
			 newcommitments.length == 0 || outstandingcommitments.length == 0 || balance.length == 0) {
		swal({
			  title: "Some Fields Empty!",
			  text: "Fill all fields !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
    
        var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!payments.match(amountRegEx)) {
		swal({
			  title: "Payments entered are invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
         
        var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!cumulativepayments.match(amountRegEx)) {
		swal({
			  title: "cumulative payments entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
              
        var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!newcommitments.match(amountRegEx)) {
		swal({
			  title: "new commitments entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
            var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!outstandingcommitments.match(amountRegEx)) {
		swal({
			  title: "outstanding commitments entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
             var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!balance.match(amountRegEx)) {
		swal({
			  title: "balance entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        var codeRegEx = /^[0-9]+$/;
         if (!itemcode.match(codeRegEx)) {
		swal({
			  title: " itemCodes entered are invalid!",
			  text: "Enter the correct item codes !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
            var codeRegEx = /^[0-9]+$/;
         if (!itemsubcode.match(codeRegEx)) {
		swal({
			  title: " itemSubCodes entered are invalid!",
			  text: "Enter the correct itemSub codes !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
           var descriptionRegEx = /^[a-zA-Z][a-zA-Z\s]*$/;
         if (!itemdescription.match(descriptionRegEx)) {
		swal({
			  title: " The description entered is invalid!",
			  text: "Enter the correct description!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        
  
  
        mess.innerHTML = "";
	mess2.innerHTML = "registered awaiting approval";
	return true;
}

function validatefacilitycollections(){
        var Date = document.getElementById('Date').value; 
        var BedandProcedures = document.getElementById('BedandProcedures').value;
	var Maternity = document.getElementById('Maternity').value;
	var Xray = document.getElementById('Xray').value; 
        var Lab = document.getElementById('Lab').value;
        var Theatre = document.getElementById('Theatre').value;
	var Mortuary = document.getElementById('Mortuary').value;
        var OPTreatment = document.getElementById('OPTreatment').value;
        var Pharmacy = document.getElementById('Pharmacy').value;
	var mess = document.getElementById('message');
	var mess2 = document.getElementById('message2');

	if ( Date.length == 0 || BedandProcedures.length == 0 || Maternity.length == 0 || 
			 Xray.length == 0 || Lab.length == 0 ||
			 Theatre.length == 0 || Mortuary.length == 0 || OPTreatment.length == 0 ||
                         Pharmacy.length == 0) {
		swal({
			  title: "Some Fields Empty!",
			  text: "Fill all fields !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
    
        var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if ( !BedandProcedures.match(amountRegEx)) {
		swal({
			  title: "Bed and Procedures entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        
            var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!Maternity.match(amountRegEx)) {
		swal({
			  title: "Maternity entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
                   var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!Xray.match(amountRegEx)) {
		swal({
			  title: "Xray entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
               var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!Lab.match(amountRegEx)) {
		swal({
			  title: "Lab entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
           var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!Theatre.match(amountRegEx)) {
		swal({
			  title: "Theatre entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!Mortuary.match(amountRegEx)) {
		swal({
			  title: "Mortuary entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
            var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!OPTreatment.match(amountRegEx)) {
		swal({
			  title: "OPTreatment entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
               var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!Pharmacy.match(amountRegEx)) {
		swal({
			  title: "Pharmacy entered is invalid!",
			  text: "Enter the correct Amounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        mess.innerHTML = "";
	mess2.innerHTML = "";
	return true;
}

function validatebankreconciliation(){
        var date = document.getElementById('date').value; 
        var payee = document.getElementById('payee').value;
	var amount = document.getElementById('amount').value;
	var details = document.getElementById('details').value; 
        var mess = document.getElementById('message');
	var mess2 = document.getElementById('message2');

	if ( date.length == 0 || payee.length == 0 || amount.length == 0 || 
			 details.length == 0) {
		swal({
			  title: "Some Fields Empty!",
			  text: "Fill all fields !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
    
        var amountRegEx= /^[1-9]\d*(?:\.\d{0,2})?$/;
        if (!amount.match(amountRegEx)) 
                                {
		swal({
			  title: "Amounts entered are invalid!",
			  text: "Enter the correct Ammounts !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        var payeeRegEx = /^[a-zA-Z][a-zA-Z\s]*$/; 
           if (!payee.match(payeeRegEx)) 
                                {
		swal({
			  title: "payee entered are invalid!",
			  text: "Enter the correct details !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
           var payeeRegEx = /^[a-zA-Z][a-zA-Z\s]*$/; 
           if (!details.match(payeeRegEx) ) 
                                {
		swal({
			  title: "Details entered are invalid!",
			  text: "Enter the correct details !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
  
        mess.innerHTML = "";
	mess2.innerHTML = "registered awaiting approval";
	return true;
}
function validatefacility(){
        var FacilityID = document.getElementById('FacilityID').value; 
        var FacilityName = document.getElementById('FacilityName').value;
	var mess = document.getElementById('message');
	var mess2 = document.getElementById('message2');

	if ( FacilityID.length == 0 || FacilityName.length == 0) {
		swal({
			  title: "Some Fields Empty!",
			  text: "Fill all fields !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
    
    	var FacilityidRegEx = /^(?=.*[a-zA-Z])(?=.*[0-9])/;
	if (FacilityID.length < 6 || !FacilityID.match(FacilityidRegEx)) {
		swal({
			  title: "Facility ID invalid!",
			  text: "Enter a valid Facility ID!",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
        var facilitynameRegEx =/^[a-zA-Z][a-zA-Z\s]*$/; 
           if (!FacilityName.match(facilitynameRegEx) ) 
                                {
		swal({
			  title: "The Facility name entered is invalid!",
			  text: "Enter the correct name !",
			  icon: "warning",
			  button: "Ok!",
			});
		return false;
	}
  
        mess.innerHTML = "";
	mess2.innerHTML = " ";
	return true;
}


