$(document).ready(function () {
        var table = $('#dataTable').DataTable({
            
            "paging": true,
            "processing": true,
            
           dom: 'Bfrtip',
    buttons: [{
  extend: 'pdf',
      filename: 'customized_pdf_file_name'
    }, {
      extend: 'excel',
      title: 'Customized EXCEL Title',
      filename: 'customized_excel_file_name'
    }, {
      extend: 'csv',
      filename: 'customized_csv_file_name'
    }]

    });
    
     });
   
  