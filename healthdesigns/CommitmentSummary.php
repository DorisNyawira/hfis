 
  <?php require_once 'AIHeader.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
            
         <form id="profileForm"  method="POST" action="../Database.php">
             
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
   
      <div class="modal-body">
           
  <div class="form-group mb-2">
      <h6>item code</h6>
  </div>
   <div class="form-group mx-sm-3 mb-2">
   <input type="hidden" class="form-control" id="id" name="id" value="">
    <input type="text" class="form-control" id="itemcode" name="itemcode" placeholder="itemcode"value="">
  </div>
  <div class="form-group mb-2">
      <h6>item sub code</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="itemsubcode"name="itemsubcode" placeholder="itemsubcode"value="">
  </div>
   <div class="form-group mb-2">
      <h6>Item description</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="itemdescription"name="itemdescription" placeholder="itemdescription"value="">
  </div>
               
  <div class="form-group mb-2">
      <h6>Payments</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="payments"name="payments" placeholder="payments"value="">
  </div>
   <div class="form-group mb-2">
      <h6>cumulative payments</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="cumulativepayments"name="cumulativepayments" placeholder="cumulativepayments"value="">
  </div>
   <div class="form-group mb-2">
      <h6>new commitments</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="newcommitments"name="newcommitments" placeholder="newcommitments"value="">
  </div>
   <div class="form-group mb-2">
      <h6>outstanding commitments</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="outstandingcommitments"name="outstandingcommitments" placeholder="outstandingcommitments"value="">
  </div>
  <div class="form-group mb-2">
      <h6>balance</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="balance"name="balance" placeholder="balance"value="">
  </div>
<div class="form-group mb-2">
      <h6>date</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="date" class="form-control" id="date"name="date" placeholder="date"value="">
  </div>
           </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="hidden"name="FacilityID" value="<?php if(isset($_SESSION['FacilityID'])) { echo htmlentities ($_SESSION['FacilityID']); }?>">
         <button type="submit" class="btn btn-success"  name="editCommitmentSummary" value="editCommitmentSummary">Edit</button>
      </div>
    
    </div>
  </div>
</div>
         </form>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
               <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">Facility Records</label>
                <form class="form-inline" action="CommitmentSummary.php" method="GET" onsubmit="setName()">

            </form> 
        
            </div> 
        <div class="table-responsive" style="margin-top:30px;">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
<thead>
	<tr>
		<th>Expense Item Code</th>
		<th>Expense Item Subcode</th>
                 <th>Expense Item Description</th>
                   <th >Payments</th>
                   <th>Cumulative Payments</th>
                  <th>New Commitments</th>
                   <th>Outstanding Commitments</th>
                   <th>Balance</th>
                    <th>Date</th>
                     <th>Option</th>
  
</tr>

</thead>
<tbody>
   <?php $handler->getpaymentsandcommitmentsummary();?>
</tbody>
</table>
</div>
          </div>
         </form>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
    <script>
       $('#editModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
     var id=$(opener).attr('id');
  $('#profileForm').find('[name="id"]').val(id);
  var itemcode=$(opener).attr('itemcode');
  $('#profileForm').find('[name="itemcode"]').val(itemcode);
    var itemsubcode=$(opener).attr('itemsubcode');
  $('#profileForm').find('[name="itemsubcode"]').val(itemsubcode);
   var itemdescription=$(opener).attr('itemdescription');
  $('#profileForm').find('[name="itemdescription"]').val(itemdescription);
    var payments=$(opener).attr('payments');
  $('#profileForm').find('[name="payments"]').val(payments);
   var cumulativepayments=$(opener).attr('cumulativepayments');
  $('#profileForm').find('[name="cumulativepayments"]').val(cumulativepayments);
    var newcommitments=$(opener).attr('newcommitments');
  $('#profileForm').find('[name="newcommitments"]').val(newcommitments);
   var outstandingcommitments=$(opener).attr('outstandingcommitments');
  $('#profileForm').find('[name="outstandingcommitments"]').val(outstandingcommitments);
    var balance=$(opener).attr('balance');
  $('#profileForm').find('[name="balance"]').val(balance);
  var dateofsummary=$(opener).attr('dateofsummary');
  $('#profileForm').find('[name="dateofsummary"]').val(dateofsummary);
  
   
});
     </script>
</body>

</html>








