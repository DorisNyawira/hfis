

<?php
session_start();
    require_once "../Database.php"; 
    require_once "../php/Admin.php";
    if(!$Admin->checklogin()){
        header("Location:login.php");
    }else{
        if($_SESSION['rank'] != "AIE Holder"){
            header("Location:login.php");
        }
    }    
?>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

   <title> <?php echo $_SESSION['FacilityName']?> report </title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
    
        <div style="margin-left: 0px;">
        <!--php $Admin->getRegisterPending();?>
    </div>-->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
         
        </div>
          <?php  
   
    $date = new DateTime("now", new DateTimeZone("America/Detroit"));

    $toDate  = $date->format("Y-m-d"); 
    $fromDate =$date->modify('-1 year')->format('Y-m-d');
              
   // $newDate = date("d-m-Y", strtotime($orgDate));  
    //echo "New date format is: ".$newDate. " (MM-DD-YYYY)";  
?>  
          <div class="sidebar-brand-text mx-3"><b>HFIS</b></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
     <li class="nav-item">
         <a class="nav-link" href="AIreports.php?FacilityID=<?php echo $_SESSION['FacilityID']?>&fromDate=<?php echo $fromDate ?>&toDate=<?php echo $toDate ?>">
            <span>Print Report</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading"><b>
              FACILITY RECORDS</b>
      </div>
  <!-- Nav Item - requests -->
      <li class="nav-item">
          <a class="nav-link" href="CommitmentSummary.php?FacilityID=<?php if(!empty($_SESSION['FacilityID'])){
    echo $_SESSION['FacilityID'];} else{echo null ;} ?>">
            <span>Commitment Summary</span></a>
      </li>
       
       <hr class="sidebar-divider">
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
         
          <span>Alter Facility Records</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="addCommitmentSummaryah.php">Commitment Summary</a>
          </div>
        </div>
      </li>
     
      
       <hr class="sidebar-divider">
           <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseone" aria-expanded="true" aria-controls="collapseTwo">
          
          <span>Approved Commitment Records</span>
        </a>
        <div id="collapseone" class="collapse" aria-labelledby="headingone" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="CommitmentSummaryApprovedah.php?FacilityID=<?php echo $_SESSION['FacilityID']?>&fromDate=<?php echo $fromDate ?>&toDate=<?php echo $toDate ?>"> committment Summary</a>
          </div>
        </div>
      </li>
       <hr class="sidebar-divider">
          <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          
          <span>    Declined Committment Records</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="CommitmentSummaryDeclinedah.php?FacilityID=<?php if(!empty($_SESSION['FacilityID'])){
    echo $_SESSION['FacilityID'];} else{echo null;} ?>"> commitment summary</a>
          </div>
        </div>
      </li>
      
       
      <!-- Heading -->
  
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
     

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>

<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <form class="form-inline">
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>
          </form>

          <!-- Topbar Search -->
       <a href="AIhelp.php"><button class="btn btn-primary pull-right" style="position:absolute;right:200px;bottom:20px;">
       <i class="fas fa-question-circle">Help</i>
             </button></a>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

        

            <div class="topbar-divider d-none d-sm-block"></div>

          <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Account Settings</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="AIuserprofile.php?IDNumber=<?php if(!empty($_SESSION['IDNUMBER'])){
    echo $_SESSION['IDNUMBER'];} else{echo null ;} ?>">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
            
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            
            
<!-- Approve and decline modals -->

<!-- Modal -->
   
      
     
 
      <!-- End of Footer -->

  
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

   <form method="post" action="../utility.php">
            
           
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-danger" type="submit" name="logout">Logout</button>
         
        </div>
          
      </div>
    </div>
  </div> 
     </form>