<?php require_once 'FacilityAccountHeader.php'?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
 <div class="card shadow mb-4">
            <div class="card-header py-3">
     
       
          

      <form action="../Database.php" method = "POST" onsubmit="return validatebankreconciliation()">
             <label style="margin-left:400px;"><b> BANK RECONCILIATION</b> </label><br><br>        
         <div class="row">
                
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    
                    <div class="col-lg-10 col-md-14 col-sm-14 col-xs-14">
                    
                         <div class="row">
                             
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                    <label>Date</label>
                                              <input type="date" name="date" id="date" class="form-control" placeholder="Enter the Date">
                                            <input type="hidden" name="FacilityID" value="<?php echo $_SESSION['FacilityID']?>"/>
                                              
                                      </div>	
                                
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Payee</label>
                                              <input type="text" name="payee" id="payee" class="form-control" placeholder="Enter the Payee">
                                             
                                      </div>	
                                </div>
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Amount</label>
                                                <input type="text" name="amount" id="amount" class="form-control" placeholder="Enter the Amount">
                                                
                                        </div>
                                    
                                </div>

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Details</label>
                                                <input type="text" name="details" id="details" class="form-control" placeholder="Enter the details">
                                               <input type="hidden" name="approval_id" class="form-control" value="0" >  
                                        </div>
                                    
                                </div>

                               
                               </div>
                    <div class="button">
                    <div style="align: center;" class="pull-right">
                    <input type="submit" class="btn btn-success" value="Submit" name="addbankreconciliation"></input>
                   
                    </div>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
                  
        </form>

        </div>
        <!-- /.container-fluid -->

      </div>
        </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../fis/index.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/validate.js"></script>
  <script src="js/alerts.js"></script>
   
</body>

</html>







