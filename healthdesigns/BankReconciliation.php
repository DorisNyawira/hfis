  
<?php require_once 'FacilityAccountHeader.php'?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
           
 <form id="profileForm"  method="POST" action="../Database.php">
    
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
   
    <div class="modal-body">
      
  <div class="form-group mb-2">
      <h6>date</h6>
  </div>
   <div class="form-group mx-sm-3 mb-2">
   <input type="hidden" class="form-control" id="id" name="id" value="">
    <input type="date" class="form-control" id="date" name="date" placeholder="date"value="">
  </div>
  <div class="form-group mb-2">
      <h6>payee</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="payee"name="payee" placeholder="payee"value="">
  </div>
             <div class="form-group mb-2">
      <h6>amount</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="amount"name="amount" placeholder="amount"value="">
  </div>
               
  <div class="form-group mb-2">
      <h6>details</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="details"name="details" placeholder="details"value="">
  </div>
   
 
           </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="hidden"name="FacilityID" value="<?php if(isset($_SESSION['FacilityID'])) { echo htmlentities ($_SESSION['FacilityID']); }?>">
         <button type="submit" class="btn btn-success"  name="editBankReconciliation" value="editBankReconciliation">Edit</button>
      </div>
    
    </div>
  </div>
</div>
 </form>
  <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">BANK RECONCILIATION RECORDS</label>
                <form class="form-inline" action="BankReconciliation.php" method="GET" onsubmit="setName()" >
                      

                   </div>  
        <div class="table-responsive" style="margin-top:30px;">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>
<tr>
	
   
   <th>date</th>
    <th>payee</th>
   <th>amount</th>
    <th>details</th>
     <th>Option</th>
   
</tr>
</thead>
<tbody>
    <?php $handler->getBankReconciliation(); ?>
</tbody>

</table>
</div>
          </div>
 </form>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
   
       <script>
       $('#editModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
     var id=$(opener).attr('id');
  $('#profileForm').find('[name="id"]').val(id);
  var date=$(opener).attr('date');
  $('#profileForm').find('[name="date"]').val(date);
    var payee=$(opener).attr('payee');
  $('#profileForm').find('[name="payee"]').val(payee);
   var amount=$(opener).attr('amount');
  $('#profileForm').find('[name="amount"]').val(amount);
    var details=$(opener).attr('details');
  $('#profileForm').find('[name="details"]').val(details);
});
     </script>
</body>

</html>







