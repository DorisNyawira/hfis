 
   
<?php require_once 'FacilityAccountHeader.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
 <div class="card shadow mb-4">
            <div class="card-header py-3">
        <script>
            function setName(){
                var selected = document.getElementById('facilityName').selectedIndex;
                localStorage.setItem("sel",selected);
            }
            
            function getName(){
                var selected = localStorage.getItem("sel");
                document.getElementById('facilityName').selectedIndex = selected;
            }
            getName();
        </script>
       
          

      <form action="../Database.php" method = "POST" onsubmit="return validatecashbook()">
          
          <label style="margin-left:400px;"><b>CASH BOOK</b> </label><br><br>
                
       <div class="row">
                
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    
                    <div class="col-lg-0 col-md-14 col-sm-14 col-xs-14">
                    <div class="jumbotron-fluid">

                                    
                               
                         <div class="row">
                              <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Bank Statement Date</label>
                                            <input type="hidden" name="FacilityID" value="<?php echo $_SESSION['FacilityID']?>"/>
                                                <input type="date"name="bankstmtdate" id="bankstmtdate" class="form-control"  placeholder="Enter the bank statement date">
                                                   
                                            </div>
                              </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Cash Book Balance</label>
                                              <input type="text" name="cashbookbalance" id="cashbookbalance" class="form-control" placeholder="Enter the cash book balance">
                                           
                                      </div>	
                                </div>
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                    <label>Total Deposits</label>
                                              <input type="text" name="totaldeposits" id="totaldeposits" class="form-control" placeholder="Enter Total Deposits">
                                              
                                      </div>	
                                
          

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Expenditures</label>
                                                <input type="text" name="expenditures" id="expenditures" class="form-control" placeholder="Enter the expenditures">
                                               
                                        </div>
                                    
                                </div>

                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Bank Charges</label>
                                                <input type="text" name="bankcharges" id="bankcharges" class="form-control"  placeholder="Enter the bank charges">
                                                   
                                             
                                            </div>
                                    
                                </div>  
        
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Total Withdrawals</label>
                                                <input type="text" name="totalwithdrawals" id="totalwithdrawals" class="form-control"  placeholder="Enter the total withdrawals">
                                               
                                                
                                            </div>
                                    
                                </div>                                
                                 </div>
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                                <label>Bank Statement Balance</label>
                                               
                                                <input type="text"name="bankstmtbalance" class="form-control" id="bankstmtbalance" placeholder="Enter The bank Statement Balance">
                                                <input type="hidden" name="approval_id" class="form-control" value="0" >    
                                        </div>  
                                      
                                </div>
          
                            </div>
                    <div class="button">
                    <div style="align: center;" class="pull-right">
                    <input type="submit" class="btn btn-success" value="Submit" name="addcashbook"></input>
                   
                    </div>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
                  
        </form>

        </div>
        <!-- /.container-fluid -->

      </div>
        </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/validate.js"></script>
  <script src="js/alerts.js"></script>
</body>

</html>






