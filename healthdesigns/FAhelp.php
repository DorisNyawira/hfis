
       <?php require_once 'FacilityAccountHeader.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
              
                <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
  <div class="container">
  <div class="row">
    <div class="col-lg-6">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5 class="panel-title">
			  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
				How to add Facility Records
			  </a><hr>
			</h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			  <p>Click on the "alter facility Records" button then click on any of the records that you want to add, it will prompt you with a page where you can add the records.</p>
			
			</div>
         <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5 class="panel-title">
			  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
				How do i edit records incase of input error
			  </a><hr>
			</h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			<p>Click on the "Facility Collections" button or the above records depending on the records you were adding , it will display a page. click on the edit button,
                            alter the incorrect date then click on edit , it will rectify the mistake.
        </div>
         <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5 class="panel-title">
			  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
				How do i know  if the records have been approved.
			  </a><hr>
			</h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			<p>click on the approved records button , then select any of the records that you will want to confirm, it will display all the records that have been 
                        approved by the Ministry of Health Or Project Manager Officer.</p>
			
			</div>
        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5 class="panel-title">
			  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
				How do i know if the records have been rejected
			  </a><hr>
			</h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			<p>Click on the "declined records" button ,then select any of the records provided , it will display all the records that have been declined
                        following their reasons.</p> 
			
			</div>
        
    </div>
    <div class="col-lg-6">
      <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5>
			  <a class="collapsed" role="button" title="" data-toggle="collapse" href="#collapse0">
				How to get the reports 
			  </a><hr>
                        </h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			<p> click on the "reports" button , it will display a page with all the approved records.click the 'pdf' or 'excel' button to download 
                        </p>
			
			</div>
	
        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5>
			  <a class="collapsed" role="button" title="" data-toggle="collapse" href="#collapse0">
				How to change your account settings
			  </a><hr>
                        </h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			<p> click on the top left button named"Account settings" and then change your name or password to your preference.
                        Submit by clicking edit profile.
                        Use the new credentials next time you will be signing in to the system.</p>
			
			</div>
         <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h5>
			  <a class="collapsed" role="button" title="" data-toggle="collapse" href="#collapse0">
				How do i log out of the system
			  </a><hr>
                        </h5>
		  </div>
        <div class="panel-body px-3 mb-4">
			<p> click on the top left button named"Account settings" and then select logout button.it will redirect you to the home page. 
                        </p>
			
    </div>
 
      
      
      
      
  </div>
</section>
                
  

            </div> 
     
          </div>

        </div>
        <!-- /.container-fluid -->

  
      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  
       <script>
            
        $('#declineModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var declineid=$(opener).attr('declineid');

//set what we got to our form
  $('#profileForm').find('[name="declineid"]').val(declineid);
   
});
            
        $('#approveModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var approveid=$(opener).attr('approveid');

//set what we got to our form
  $('#Approvalform').find('[name="approveid"]').val(approveid);
   
});
            
             </script>
             

</body>

      </html>





