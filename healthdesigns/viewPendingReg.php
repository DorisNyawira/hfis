<?php require_once 'adminheader.php'?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
    <div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
              <h6 class="m-0 font-weight-bold text-primary">Registration Requests</h6>
            </div> 
        <div class="table-responsive">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
<thead>
                           <tr>
                        <th>IDNumber</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>JobID</th>
                         <th>Rank</th>
                         <th>Status Approve</th>
                          <th>Status Decline</th>
                        
                        </tr>
</thead>
<tbody>
   
        <?php $Admin->getRegisterPending();
                
            ?>
        
  
</tbody>
</table>
</div>
          </div>
   
 
      <!-- End of Footer -->

  
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="fis/index.html">Logout</a>
        </div>
      </div>
    </div>
  </div> 
       
          <!-- DataTales Example -->
</div>
    
        </div>
        <!-- /.container-fluid -->

      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  
       <script>
            
        $('#declineModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var declineid=$(opener).attr('declineid');

//set what we got to our form
  $('#profileForm').find('[name="declineid"]').val(declineid);
   
});
            
        $('#approveModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var approveid=$(opener).attr('approveid');

//set what we got to our form
  $('#Approvalform').find('[name="approveid"]').val(approveid);
   
});
            
             </script>
</body>

</html>

