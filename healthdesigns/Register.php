<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>HFIS</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
 
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body>
<form action='../utility.php' method='post'id="user" onsubmit="return validateRegister()"style="background-color:#3399ff;">
  <div class="container">
  <div class="row justify-content-center">
  <div class="col-xl-9 col-lg-6 col-md-8 col-xs-2">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row" style="font-size: 22px;">
       <div class="col-lg-12">
                <div class="p-3">
           <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
             
              
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="id"><b>ID Number</b></label><input class="form-control py-4" id="id" name="id" type="text" placeholder="Enter ID Number" /></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputFirstName"><b>First Name</b></label><input class="form-control py-4" id="fname"name="fname" type="text" placeholder="Enter First name" /></div>
                                                </div>
                                            </div>
                                             <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputLastName"><b>Last Name</b></label><input class="form-control py-4" id="lname" name="lname" type="text" placeholder="Enter Last name" /></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputJobID"><b>Job ID</b></label><input class="form-control py-4" id="jobid"name="jobid" type="text" placeholder="Enter Job ID " /></div>
                                                </div>
                                            </div>
                    <div class="form-group"><label class="small mb-1" for="inputEmailAddress"><b>Email Address</b></label><input class="form-control py-4" id="email" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email address" /></div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputPassword"><b>Password</b></label><input class="form-control py-4" id="pass" name="pass" type="password" placeholder="Enter password" /></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputConfirmPassword"><b>Confirm Password</b></label><input class="form-control py-4" id="cpass"name="confirm" type="password" placeholder="Confirm password" /></div>
                                                </div>
                                            </div>
                                           
                                      
                                            <div class="text-center">
                 
                <a class="small" href="login.php" ><input type="submit" name='register' value="Create Account" style="background-color:#3399ff;width:250px;height:40px;font-size:20px;color:white;"><br>
                 
                </a>
                                            </div>
              <hr>
            
              <div class="text-center">
                  <a class="small" href="login.php">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</form>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="js/validate.js"></script>
  <script src="js/alerts.js"></script>
</body>
<?php  include_once 'footer.php'; ?>
</html>

