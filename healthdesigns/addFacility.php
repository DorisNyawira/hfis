<?php require_once 'adminheader.php'?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
    <div class="card shadow mb-4" >
            <div class="card-header py-3" style="margin-bottom: 10px;">
              <h6 class="m-0 font-weight-bold text-primary">New Facility</h6>
            </div>
        <form method="POST" action="../Database.php" onsubmit="return validatefacility()">
        <div class="row" style="margin-left:20px;">
                           
                    
                  
                  
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Facility ID</label>
                                              <input type="text" name="FacilityID" id="FacilityID" class="form-control" placeholder="Enter the Facility ID">
                                           
                                      </div>	
                                </div>
          
                               
                                
          
                                <div class="col-xm-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                      <div class="form-group">
                                              <label>Facility Name</label>
                                              <input type="text" name="FacilityName" id="FacilityName" class="form-control" placeholder="Enter Facility Name">
                                             
                                      </div>	
                                </div>
      
                        
   <div class="button">
                    <div style="position:absolute; right:30px;bottom:10px;" class="pull-right">
                    <input type="submit" class="btn btn-success" value="Submit" name="addFacility"></input>
                  
                    </div>
   </div>
                       
                    </div>
        </form>
       
      <!-- End of Footer -->

  
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="fis/index.html">Logout</a>
        </div>
      </div>
    </div>
  </div> 
       
          <!-- DataTales Example -->
</div>
    
        </div>
        <!-- /.container-fluid -->

      <!-- End of Main Content -->
      
           <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
     
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
    <script src="js/validate.js"></script>
     <script src="js/alerts.js"></script>
  
       <script>
            
        $('#declineModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var declineid=$(opener).attr('declineid');

//set what we got to our form
  $('#profileForm').find('[name="declineid"]').val(declineid);
   
});
            
        $('#approveModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
  var approveid=$(opener).attr('approveid');

//set what we got to our form
  $('#Approvalform').find('[name="approveid"]').val(approveid);
   
});
            
             </script>
</body>

</html>
   



