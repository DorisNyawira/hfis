 
  
<?php require_once 'FacilityAccountHeader.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <form id="profileForm"  method="POST" action="../Database.php">
           
  
  <!--Modal: Contact form-->
  

    <!--Content-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
   
    <div class="modal-body">
      
  <div class="form-group mb-2">
      <h6>bank statement date</h6>
  </div>
   <div class="form-group mx-sm-3 mb-2">
   <input type="hidden" class="form-control" id="id" name="id" value="">
    <input type="date" class="form-control" id="bankstmtdate" name="bankstmtdate" placeholder="bankstmtdate"value="">
  </div>
  <div class="form-group mb-2">
      <h6>cash book balance</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="cashbookbalance"name="cashbookbalance" placeholder="cashbookbalance"value="">
  </div>
             <div class="form-group mb-2">
      <h6>total deposits</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="totaldeposits"name="totaldeposits" placeholder="totaldeposits"value="">
  </div>
               
  <div class="form-group mb-2">
      <h6>expenditures</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="expenditures"name="expenditures" placeholder="expenditures"value="">
  </div>
   <div class="form-group mb-2">
      <h6>bank charges</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="bankcharges"name="bankcharges" placeholder="bankcharges"value="">
  </div>
   <div class="form-group mb-2">
      <h6>total withdrawals</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="totalwithdrawals"name="totalwithdrawals" placeholder="totalwithdrawals"value="">
  </div>
  <div class="form-group mb-2">
      <h6>bank statement balance</h6>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" id="bankstmtbalance"name="bankstmtbalance" placeholder="bankstmtbalance"value="">
  </div>

           </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="hidden"name="FacilityID" value="<?php if(isset($_SESSION['FacilityID'])) { echo htmlentities ($_SESSION['FacilityID']); }?>">
         <button type="submit" class="btn btn-success"  name="editCashBook" value="editCashBook">Edit</button>
      </div>
    
    </div>
  </div>
</div>
        </form>
  
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
               <div class="card-header py-3" style="margin-bottom: 10px;">
                <label id="FacilityRecords" class="m-0 font-weight-bold text-primary">CASH BOOK RECORDS</label>
                <form class="form-inline" action="CashBook.php?FacilityID=<?php echo $_SESSION['FacilityID']?>" method="GET" onsubmit="setName()">
            </form> 
             
            </div>
        <div class="table-responsive" style="margin-top:30px;">  
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">  
  
  
<thead>
	
<tr>
     <th>bank stmt date</th>
    <th>cash book balance</th>
   <th>total deposits</th>
   <th>expenditures</th>
   <th>PMO/MOH cheque</th>
   <th>total withdrawals</th>
  <th>bank stmt balance</th>
   <th>Option</th>

</tr>
</thead>
<tbody>
    <?php $handler->getcashbook(); ?>
</tbody>
</table>
</div>
          </div>
     
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
   <script>
       $('#editModal').on('show.bs.modal', function (e) {
  // get information to update quickly to modal view as loading begins
  var opener=e.relatedTarget;//this holds the element who called the modal
   
   //we get details from attributes
     var id=$(opener).attr('id');
  $('#profileForm').find('[name="id"]').val(id);
  var bankstmtdate=$(opener).attr('bankstmtdate');
  $('#profileForm').find('[name="bankstmtdate"]').val(bankstmtdate);
    var cashbookbalance=$(opener).attr('cashbookbalance');
  $('#profileForm').find('[name="cashbookbalance"]').val(cashbookbalance);
   var totaldeposits=$(opener).attr('totaldeposits');
  $('#profileForm').find('[name="totaldeposits"]').val(totaldeposits);
    var expenditures=$(opener).attr('expenditures');
  $('#profileForm').find('[name="expenditures"]').val(expenditures);
   var bankcharges=$(opener).attr('bankcharges');
  $('#profileForm').find('[name="bankcharges"]').val(bankcharges);
    var totalwithdrawals=$(opener).attr('totalwithdrawals');
  $('#profileForm').find('[name="totalwithdrawals"]').val(totalwithdrawals);
   var bankstmtbalance=$(opener).attr('bankstmtbalance');
  $('#profileForm').find('[name="bankstmtbalance"]').val(bankstmtbalance);
});
     </script>
</body>

</html>






